package com.social.base.utils;

import android.content.Context;
import android.location.Location;
import android.provider.Settings;

public class LocationUtil {

    public static boolean isMockLocation(Context context, Location location) {

        return AndroidVersionUtil.INSTANCE.hasKitKat() ? location.isFromMockProvider()
                : !Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ALLOW_MOCK_LOCATION).equals("0");
    }
}
