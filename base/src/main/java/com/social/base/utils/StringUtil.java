package com.social.base.utils;

import android.graphics.Typeface;
import android.text.ParcelableSpan;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

    public static Spannable spannableSizeInMessage(String message, float size, String... wordsToFind) {
        Spannable spannable;
        if (!TextUtils.isEmpty(message)) {
            Pattern pattern;
            Matcher match;
            spannable = new SpannableString(message);
            RelativeSizeSpan textSizeSpan;
            StyleSpan styleSpan;
            for (String word : wordsToFind) {

                pattern = Pattern.compile(word, Pattern.CASE_INSENSITIVE);
                match = pattern.matcher(message);

                if (match.find()) {
                    textSizeSpan = new RelativeSizeSpan(size);
                    styleSpan = new StyleSpan(Typeface.BOLD);
                    spannable.setSpan(textSizeSpan, match.start(), match.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spannable.setSpan(styleSpan, match.start(), match.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        } else {
            spannable = new SpannableString("");
        }
        return spannable;
    }

    public static Spannable setSpannablesFromRegex(CharSequence completeString, String regex, ParcelableSpan... spans) {
        Spannable spannable = new SpannableString("");
        if (!TextUtils.isEmpty(completeString)) {
            Pattern pattern;
            Matcher match;
            spannable = new SpannableString(completeString);
            pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            match = pattern.matcher(completeString);
            while (match.find()) {
                for (ParcelableSpan span : spans) {
                    spannable.setSpan(span, match.start(), match.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        }
            return spannable;
    }

    public static Spannable applySpans(CharSequence text, ParcelableSpan... spans) {
        Spannable result = new SpannableString("");
        if (!TextUtils.isEmpty(text)) {
            result = new SpannableString(text);
            for (ParcelableSpan span : spans) {
                if (span != null) {
                    result.setSpan(span, 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        }
        return result;
    }

    public static boolean isEmpty(String text) {
        return text == null || text.trim().isEmpty() || text.equalsIgnoreCase("null");
    }

    public static boolean isEmailValid(String email) {
        boolean emailValid = false;
        if (!isEmpty(email)) {
            Pattern pattern = Pattern.compile("^.+@.+\\..+$");
            Matcher matcher = pattern.matcher(email);
            emailValid = matcher.find();
        }
        return emailValid;
    }
}
