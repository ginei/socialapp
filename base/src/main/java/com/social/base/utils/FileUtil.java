package com.social.base.utils;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.core.content.FileProvider;

import com.social.base.R;

import java.io.File;


public class FileUtil {

    private static String appName;
    private static String applicationId;
    private static final String USER = "user";

    public static String pathLastFileGenerated;

    public static void init(String applicationId) {
        FileUtil.applicationId = applicationId;
    }

    public static void generateBasicFolders(String appName) {
        FileUtil.appName = appName;
        createFolder(Environment.getExternalStorageDirectory() + File.separator + appName);
        createFolder(getPathForUserFolder());
    }

    private static void createFolder(String path) {
        File folder = new File(path);
        if (!folder.exists()) {
            folder.mkdirs();
        }
    }

    public static Uri generateUriForPicture(Context context, String folderPath) {
        String name = String.valueOf(System.currentTimeMillis()) + ".jpg";
        File file = new File(folderPath, name);
        pathLastFileGenerated = folderPath + name;
        FileUtil.generateBasicFolders(context.getString(R.string.app_name));
        return FileProvider.getUriForFile(context, applicationId + ".provider", file);
    }

    public static String getPathForUserFolder() {
        return Environment.getExternalStorageDirectory() + File.separator + appName + File.separator + USER + File.separator;
    }

    public static File getLastFileModifiedOnFolder(String folderPath) {
        File folder = new File(folderPath);
        File[] files = folder.listFiles(File::isFile);
        long lastMod = Long.MIN_VALUE;
        File choice = null;
        if (files != null) {
            for (File file : files) {
                if (file.lastModified() > lastMod) {
                    choice = file;
                    lastMod = file.lastModified();
                }
            }
        }
        return choice;
    }

    public static File getPhotoToGallery(Context context, Intent data) {
        Uri selectedImage = data.getData();
        String[] filePath = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(selectedImage, filePath, null, null, null);
        cursor.moveToFirst();
        String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
        return new File(imagePath);
    }

}
