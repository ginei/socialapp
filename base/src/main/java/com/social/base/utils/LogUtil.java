package com.social.base.utils;

import android.text.TextUtils;
import android.util.Log;

public class LogUtil {

    private static final boolean DEBUG = true;

    public static void e(String key, String message) {
        if (DEBUG && !TextUtils.isEmpty(key) && !TextUtils.isEmpty(message)) {
            Log.e(key, message);
        }
    }

    public static void v(String key, String message) {
        if (DEBUG && !TextUtils.isEmpty(key) && !TextUtils.isEmpty(message)) {
            Log.v(key, message);
        }
    }

    public static void d(String key, String message) {
        if (DEBUG && !TextUtils.isEmpty(key) && !TextUtils.isEmpty(message)) {
            Log.d(key, message);
        }
    }

    public static void net(String key, String message) {
        if (DEBUG && !TextUtils.isEmpty(key) && !TextUtils.isEmpty(message)) {
            Log.v(key, message);
        }
    }
}