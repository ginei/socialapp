package com.social.base.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class DateUtils {

    public static final String DEFAULT_FORMAT_DATE = "yyyy-MM-dd";
    public static final String DEFAULT_FORMAT_DATE_HOURS = "yyyy-MM-dd HH:mm:ss";
    public static final String READABLE_FORMAT_DATE = "d MMM yyyy hh:mm a";
    public static final String DAY_OF_WEEK_LONG = "EEE";
    public static final String ISO_8601 = "yyyy-MM-dd'T'HH:mm:ss";

    public static String toCalendar(final String iso8601string, String format)
            throws ParseException {
        Calendar calendar = GregorianCalendar.getInstance();
        Date date = new SimpleDateFormat(ISO_8601, Locale.getDefault()).parse(iso8601string);
        calendar.setTime(date);
        return calendarToFormat(calendar, format);
    }

    private static String calendarToFormat(Calendar calendar, String format) {
        calendar.add(Calendar.HOUR, -5);
        SimpleDateFormat format1 = new SimpleDateFormat(format, Locale.getDefault());
        return format1.format(calendar.getTime());
    }

    public static String getCurrentDate(String format, Locale locale) {
        return new SimpleDateFormat(format, locale).format(new Date());
    }

    public static String parseDateToStringWithFormat(Date date, String format) {
        return parseDateToStringWithFormat(date, format, Locale.getDefault());
    }

    public static String parseDateToStringWithFormat(Date date, String format, Locale locale) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, locale);
        return sdf.format(date);
    }

    public static String getReadableDate(String date) {
        return getReadableDate(date, DEFAULT_FORMAT_DATE_HOURS, READABLE_FORMAT_DATE);
    }

    public static String getReadableDate(String date, String actualFormat, String desiredFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(desiredFormat, Locale.getDefault());
        DateFormat serverFormat = new SimpleDateFormat(actualFormat, Locale.ENGLISH);
        String dateToReturn = "";
        try {
            dateToReturn = sdf.format(serverFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateToReturn;
    }

    public static long getDateMillisecondsForAlarm(Date date, int eta) {
        if (date != null) {
            long millisecondsStart = date.getTime();
            long millisecondsEta = 1000 * 60 * eta;

            return millisecondsStart + millisecondsEta;
        }
        return 0;
    }

    public static boolean millisecondsAfterActualTime(long milliseconds) {
        long actualMilliseconds = new Date().getTime();
        return !(actualMilliseconds > milliseconds);
    }

    public static String getDateWithAdditionalDay(String dateString, int additionalDay) {
        SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_FORMAT_DATE, Locale.getDefault());
        try {
            Calendar cal = Calendar.getInstance();
            cal.setTime(sdf.parse(dateString));
            cal.add(Calendar.DATE, additionalDay);
            dateString = sdf.format(cal.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }


}
