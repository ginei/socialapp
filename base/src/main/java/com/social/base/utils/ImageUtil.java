package com.social.base.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;

import java.io.File;
import java.io.FileOutputStream;

public class ImageUtil {

    public static final String IMAGE_JPG = "image/jpeg";

    public static Drawable getDrawableVector(Context context, int srcImage) {
        return AndroidVersionUtil.INSTANCE.hasLollipop() ? context.getResources().getDrawable(srcImage, context.getTheme()) :
                VectorDrawableCompat.create(context.getResources(), srcImage, context.getTheme());
    }

    private static final String REDUCED_PREFIX = "reduced_";
    private static final String STOREKEEPER_ID = "SKID_";

    /**
     * the final image is going to have the same name as the original file but with the reduced prefix
     */
    public static File resizeImage(File file, int newWidth, int newHeight) {
        Bitmap scaledBitmap;
        File finalFile = null;

        try {
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(file.getAbsolutePath(), newWidth, newHeight, ScalingUtilities.ScalingLogic.FIT);

            if (!(unscaledBitmap.getWidth() <= newWidth && unscaledBitmap.getHeight() <= newHeight)) {
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, newWidth, newHeight, ScalingUtilities.ScalingLogic.FIT);

                finalFile = new File(file.getParentFile().getAbsolutePath(), REDUCED_PREFIX + file.getName().trim());

                FileOutputStream fileOutputStream;

                fileOutputStream = new FileOutputStream(finalFile);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();

                scaledBitmap.recycle();

            } else {
                unscaledBitmap.recycle();
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }

        if (finalFile == null) {
            finalFile = file;
        } else {
            file.delete();
        }

        return finalFile;

    }
}
