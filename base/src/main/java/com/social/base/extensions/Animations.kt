package com.social.base.extensions

import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.view.View
import android.view.animation.OvershootInterpolator

const val MEDIUM_DURATION = 300
const val SHORT_MEDIUM_DURATION = 200

fun View.beatAnimation(animatorListenerAdapter: AnimatorListenerAdapter? = null) {
    val overShoot = OvershootInterpolator()
    val animatorSet = AnimatorSet()
    val bounceAnimX = ObjectAnimator.ofFloat(this, "scaleX", 0.2f, 1f)
    bounceAnimX.duration = MEDIUM_DURATION.toLong()
    bounceAnimX.interpolator = overShoot

    val bounceAnimY = ObjectAnimator.ofFloat(this, "scaleY", 0.2f, 1f)
    bounceAnimY.duration = MEDIUM_DURATION.toLong()
    bounceAnimY.interpolator = overShoot

    animatorSet.playTogether(bounceAnimX, bounceAnimY)
    animatorSet.start()
}