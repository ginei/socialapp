package com.social.base.ui.interfaces;

public interface GenericCategoryItem<T> extends GenericItem<T> {

    String getCategoryName();

    int compareTo(GenericCategoryItem categoryItem);

}
