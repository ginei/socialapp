package com.social.base.ui;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;
import com.social.base.ui.factories.SnackBarFactory;
import com.social.base.ui.views.Loading;


public abstract class BaseFragment extends Fragment {

    protected Loading loadingBar;

    private void initLoading() {
        if (loadingBar == null) {
            loadingBar = new Loading(getActivity());
            getActivity().addContentView(loadingBar, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        }
    }


    public void showLoading(Boolean showing) {
        initLoading();
        if (showing) {
            loadingBar.showProgressBar();
        } else {
            loadingBar.hideProgressBar();
        }
    }

    private void showMessage(@SnackBarFactory.SnackBarType String type, @NonNull View view, String message, int duration) {
        SnackBarFactory.getSnackBar(type, view, message, duration).show();
    }

    public void showError(@NonNull View view, String message) {
        showMessage(SnackBarFactory.TYPE_ERROR, view, message, Snackbar.LENGTH_LONG);
    }

    public void showInfoMessage(View view, String message) {
        showMessage(SnackBarFactory.TYPE_INFO, view, message, Snackbar.LENGTH_LONG);
    }

    public String getName() {
        return this.getClass().getName();
    }

    public int getEnter() {
        return 0;
    }

    public int getExit() {
        return 0;
    }

    public int getPopEnter() {
        return 0;
    }

    public int getPopExit() {
        return 0;
    }

    public abstract int getContainer();
}
