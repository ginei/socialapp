package com.social.base.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.snackbar.Snackbar;
import com.social.base.ui.factories.SnackBarFactory;
import com.social.base.ui.views.Loading;

import java.util.ArrayList;
import java.util.List;

public class BaseActivity extends AppCompatActivity {

    protected ProgressDialog progressDialog;

    protected Loading loadingBar;

    private boolean paused;

    private FragmentManager fragmentManager;

    private List<BaseFragment> pendingForOpen = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getSupportFragmentManager();
        initListeners();
    }

    private void initListeners() {
        fragmentManager.addOnBackStackChangedListener(() -> onBackStackChanged(fragmentManager.getBackStackEntryCount()));
    }

    protected void onBackStackChanged(int currentFragments) {
    }

    @Override
    protected void onPause() {
        super.onPause();
        paused = true;
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        paused = false;
    }

    private void initProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
        }
    }

    private void initLoading() {
        if (loadingBar == null) {
            loadingBar = new Loading(this);
            this.addContentView(loadingBar, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        }
    }

    protected void replaceFragment(BaseFragment fragment) {
        if (paused || isFinishing()) {

            pendingForOpen.add(fragment);

        } else {

            FragmentTransaction ft = fragmentManager.beginTransaction();

            if (fragmentManager.getBackStackEntryCount() >= 0) {

                ft.setCustomAnimations(fragment.getEnter(), fragment.getExit(), fragment.getPopEnter(), fragment.getPopExit());
            }

            ft.replace(fragment.getContainer(), fragment, fragment.getName());

            ft.commit();
        }
    }

    protected void addFragment(BaseFragment fragment, int containerViewId, boolean addToStack) {
        if (paused || isFinishing()) {

            pendingForOpen.add(fragment);

        } else {

            FragmentTransaction ft = fragmentManager.beginTransaction();

            if (fragmentManager.getBackStackEntryCount() >= 0) {

                ft.setCustomAnimations(fragment.getEnter(), fragment.getExit(), fragment.getPopEnter(), fragment.getPopExit());
            }

            ft.add(containerViewId, fragment, fragment.getName());

            if (addToStack) {

                ft.addToBackStack(fragment.getName());
            }

            ft.commit();
        }
    }


    public void clearBackStack() {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public void showLoading(Boolean showing) {
        initLoading();
        if (showing) {
            loadingBar.showProgressBar();
        } else {
            loadingBar.hideProgressBar();
        }
    }

    public void showProgressDialog(Pair<Boolean, Integer> progressData) {
        initProgressDialog();
        if (progressData.first) {
            progressDialog.setMessage(getString(progressData.second));
            progressDialog.show();
        } else {
            progressDialog.hide();
            progressDialog.dismiss();
        }
    }

    public void showMessage(@SnackBarFactory.SnackBarType String type, @NonNull View view, String message) {
        SnackBarFactory.getSnackBar(type, view, message, Snackbar.LENGTH_LONG);
    }

    public void showMessage(@SnackBarFactory.SnackBarType String type, @NonNull View view, String message, int duration) {
        SnackBarFactory.getSnackBar(type, view, message, duration);
    }


    public void showError(@NonNull View view, String message) {
        showMessage(SnackBarFactory.TYPE_ERROR, view, message, Snackbar.LENGTH_LONG);
    }

    public void showToast(Context context, String message) {
        Toast.makeText(context, message , Toast.LENGTH_SHORT).show();
    }

    public void showInfoMessage(View view, String message) {
        showMessage(SnackBarFactory.TYPE_INFO, view, message, Snackbar.LENGTH_LONG);
    }

}
