package com.social.base.ui.interfaces;

import com.social.base.ui.adapters.GenericAdapter;

public interface GenericItemView<T> {

    void bind(T item);

    T getData();

    void setItemClickListener(GenericAdapter.OnItemClickListener onItemClickListener);

    int getIdForClick();

}
