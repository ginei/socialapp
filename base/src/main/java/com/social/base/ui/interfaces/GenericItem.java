package com.social.base.ui.interfaces;

public interface GenericItem<T> {

    T getData();

    int getType();

    void setData(T data);
}
