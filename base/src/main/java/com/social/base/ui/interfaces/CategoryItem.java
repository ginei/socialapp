package com.social.base.ui.interfaces;


import com.social.base.ui.factories.GenericAdapterFactory;

public class CategoryItem implements GenericCategoryItem<String> {

    public String category;

    public CategoryItem(String category) {
        this.category = category;
    }

    @Override
    public String getData() {
        return category;
    }

    @Override
    public int getType() {
        return GenericAdapterFactory.TYPE_CATEGORY;
    }

    @Override
    public void setData(String data) {
        this.category = data;
    }

    @Override
    public String getCategoryName() {
        return category;
    }

    @Override
    public int compareTo(GenericCategoryItem categoryItem) {
        return category.compareTo(categoryItem.getCategoryName());
    }

}
