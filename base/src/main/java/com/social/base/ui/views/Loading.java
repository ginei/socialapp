package com.social.base.ui.views;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import com.social.base.R;
import com.social.base.databinding.ViewProgressBaseBinding;


public class Loading extends RelativeLayout {

    private ViewProgressBaseBinding binding;

    public Loading(Context context) {
        super(context);
        init();
    }

    public Loading(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Loading(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.view_progress_base, this, true);
        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white_transparent));
        setClickable(true);
    }

    public void showProgressBar() {
        this.setVisibility(View.VISIBLE);
        binding.progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        this.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.GONE);
    }

}
