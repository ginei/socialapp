-ignorewarnings
-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-verbose
-optimizations !code/simplification/arithmetic,!field
-optimizations !code/*,!field/*,!class/merging/*,!method/*

-keepclassmembers class * {
    native <methods>;
}

-keepclassmembers class * implements android.os.Parcelable {
    static *** CREATOR;
}
# The Maps Android API uses serialization.
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-dontwarn android.support.**
-dontwarn com.squareup.okhttp.**
-dontwarn javax.annotation.**
-dontwarn okio.**

# Retrofit rules
# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on RoboVM on iOS. Will not be used at runtime.
-dontnote retrofit2.Platform$IOS$MainThreadExecutor
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions

-printmapping build/outputs/mapping/release/mapping.txt

# Retrofit rules
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }

-keep class okhttp3.** { *; }
-dontwarn okhttp3.**
