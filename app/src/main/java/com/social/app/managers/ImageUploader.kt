package com.social.app.managers

import android.net.Uri

import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.storage.OnProgressListener
import com.google.firebase.storage.StorageMetadata
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.social.base.utils.LogUtil

import io.reactivex.Observable
import io.reactivex.ObservableEmitter

class ImageUploader private constructor(private val subscriber: ObservableEmitter<in String>) : OnFailureListener, OnSuccessListener<UploadTask.TaskSnapshot>, OnProgressListener<UploadTask.TaskSnapshot> {

    override fun onFailure(exception: Exception) {
        if (subscriber.isDisposed.not()) {
            LogUtil.e(ImageUploader::class.java.name, "Photo upload failed")
            subscriber.onError(exception)
        }
    }

    override fun onSuccess(taskSnapshot: UploadTask.TaskSnapshot) {
        if (subscriber.isDisposed.not()) {
            LogUtil.e(ImageUploader::class.java.name, "Photo upload onSuccess")
            val downloadUrl = taskSnapshot.storage.downloadUrl
            subscriber.onNext(downloadUrl.toString())
            subscriber.onComplete()
        }
    }

    override fun onProgress(taskSnapshot: UploadTask.TaskSnapshot) {
        LogUtil.e(ImageUploader::class.java.name, "Progress: " +
                taskSnapshot.bytesTransferred * 100 / taskSnapshot.totalByteCount)
    }

    companion object {

        fun uploadImage(uriFile: Uri, storageReference: StorageReference,
                        path: String, metadata: StorageMetadata): Observable<String> {

            return Observable.create { subscriber ->
                val uploader = ImageUploader(subscriber)
                val uploadTask = storageReference.child(path).putFile(uriFile, metadata)
                uploadTask.addOnFailureListener(uploader)
                uploadTask.addOnSuccessListener(uploader)
                uploadTask.addOnProgressListener(uploader)
            }
        }
    }
}