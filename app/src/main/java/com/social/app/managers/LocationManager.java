package com.social.app.managers;

import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.subjects.BehaviorSubject;


public class LocationManager {

    public static final double COLOMBIA_LAT = 4.624335;
    public static final double COLOMBIA_LONG = -74.063644;

    private static BehaviorSubject<Location> currentLocation = BehaviorSubject.create();

    private static GoogleApiClient googleApiClient;

    private static LocationRequest locationRequest;

    public static void setLocation(Location location) {
        currentLocation.onNext(location);
    }

    public static void setGoogleApiClient(GoogleApiClient googleApiClient) {
        LocationManager.googleApiClient = googleApiClient;
    }

    public static void setLocationRequest(LocationRequest locationRequest) {
        LocationManager.locationRequest = locationRequest;
    }

    public static double getLastLatitude() {
        if (currentLocation.getValue() != null) {
            return currentLocation.getValue().getLatitude();
        }
        return 0;
    }

    public static double getLastLongitude() {
        if (currentLocation.getValue() != null) {
            return currentLocation.getValue().getLongitude();
        }
        return 0;
    }

    public static Observable<Location> subscribeLocation() {
        return currentLocation.observeOn(AndroidSchedulers.mainThread());
    }

    public static Location getLocation() {
        return currentLocation.getValue();
    }

    public static GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }

    public static LocationRequest getLocationRequest() {
        return locationRequest;
    }
}
