package com.social.app.managers

import android.net.Uri
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageMetadata
import com.social.base.utils.DateUtils
import com.social.base.utils.ImageUtil
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.util.*

object FirebaseStorageManager {

    fun attemptToUploadPhoto(photosPath: String, scaledFile: File): Observable<String> {
        return uploadPictureToFirebase(photosPath, scaledFile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    private fun uploadPictureToFirebase(folderName: String, fileUpload: File?): Observable<String> {
        var file = fileUpload

        val storage = FirebaseStorage.getInstance()

        val storageReference = storage.reference

        val timeStamp = DateUtils.getCurrentDate("yyyyMMdd_HHmmss", Locale.getDefault())

        val metadata = StorageMetadata.Builder()
                .setContentType(ImageUtil.IMAGE_JPG)
                .setCustomMetadata("taken_date", timeStamp)
                .build()

        if (file == null) {
            file = FileManager.getLastModifiedFileOnExternalStorageInFolder(folderName)
        }

        val imageFile = Uri.fromFile(file)

        val path = file?.name?.trim()?: ""

        return ImageUploader.uploadImage(imageFile, storageReference, path, metadata)
    }

}