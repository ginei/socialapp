package com.social.app.managers

import com.google.gson.Gson
import com.social.app.api.models.ProfileResponse
import com.social.app.managers.preferences.ConstantPreferences
import com.social.app.services.LocationServiceApi
import com.social.app.utils.FirebaseUtils
import com.social.app.utils.ServiceUtils
import com.social.base.preferences.FilePreference
import com.social.base.preferences.PrefsManager
import com.social.base.utils.StringUtil

object ProfileManager {

    private var userProfile: ProfileResponse? = null

    val userId: String
        get() = getUserProfile().id

    val groupId: Int
        get() = getUserProfile().group ?: 1

    val telephone: String?
        get() = getUserProfile().phone

    val userName: String?
        get() = getUserProfile().name

    val isUserLogged: Boolean
        get() {
            val prefsManager = PrefsManager.getInstance()
            val token = prefsManager.getString(ConstantPreferences.ACCESS_TOKEN)
            return !StringUtil.isEmpty(token)
        }

    val isUserActive: Boolean
        get() = PrefsManager.getInstance().getBoolean(ConstantPreferences.IS_ACTIVATE)

    fun logoutDevice() {
        PrefsManager.getInstance().resetPreferencesLess(FilePreference.UrlInformation)
        deactivateUser()
    }

    fun activateOrDeactivateUser(state: Boolean) {
        PrefsManager.getInstance().set(ConstantPreferences.IS_ACTIVATE, state)
    }

    private fun deactivateUser() {
        activateOrDeactivateUser(false)
        ServiceUtils.stopService(LocationServiceApi::class.java)
        FirebaseUtils.getInstance().unregisterAllChannels()
    }

    fun updateUserProfile(prefsManager: PrefsManager, profileResponse: ProfileResponse) {
        prefsManager.set(ConstantPreferences.USER, Gson().toJson(profileResponse))
        FirebaseUtils.getInstance().registerClientChannel(ProfileManager.telephone.toString())
        FirebaseUtils.getInstance().registerGroupChannel(ProfileManager.groupId.toString())
        userProfile = profileResponse
    }

    fun getUserProfile(): ProfileResponse {
        if (userProfile == null) {
            userProfile = Gson().fromJson(PrefsManager.getInstance().getString(ConstantPreferences.USER), ProfileResponse::class.java)
        }
        return userProfile ?: ProfileResponse()
    }

    fun setUserLogged() {
        PrefsManager.getInstance().set(ConstantPreferences.ACCESS_TOKEN, "-")
    }

}
