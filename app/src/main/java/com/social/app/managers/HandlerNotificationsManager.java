package com.social.app.managers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.service.notification.StatusBarNotification;
import androidx.core.app.NotificationCompat;

import com.social.base.utils.AndroidVersionUtil;
import com.social.app.R;
import com.social.app.ui.GenericApplication;

public class HandlerNotificationsManager {

    private static NotificationManager getNotificationManager() {
        return (NotificationManager) GenericApplication.get().getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public static void cancelNotificationWithId(int notificationId) {
        getNotificationManager().cancel(notificationId);
    }

    public static void createNotification(int notificationId, Notification notification) {
        getNotificationManager().notify(notificationId, notification);
    }

    public static StatusBarNotification[] getActiveNotifications() {
        StatusBarNotification[] statusBarNotifications;
        if (AndroidVersionUtil.INSTANCE.hasMarshmallow()) {
            statusBarNotifications = getNotificationManager().getActiveNotifications();
        } else {
            statusBarNotifications = new StatusBarNotification[0];
        }
        return statusBarNotifications;
    }

    public static Notification getNotificationForService(Context context, String title, String message, PendingIntent pendingIntent, String channelId) {
        try {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context,channelId)
                    .setContentTitle(title)
                    .setChannelId(channelId)
                    .setTicker(title)
                    .setContentText(message)
                    .setContentIntent(pendingIntent)
                    .setOngoing(true);
            if (AndroidVersionUtil.INSTANCE.hasLollipop()) {
                builder.setSmallIcon(R.drawable.logo_social);
            } else {
                builder.setSmallIcon(R.drawable.logo_social);
            }

            return builder.build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
