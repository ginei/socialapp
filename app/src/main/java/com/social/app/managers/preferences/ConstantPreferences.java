package com.social.app.managers.preferences;

import com.social.base.preferences.FilePreference;
import com.social.base.preferences.PreferencesKey;

public class ConstantPreferences {

    public static PreferencesKey USER = new PreferencesKey("user", "");
    public static PreferencesKey IS_ACTIVATE = new PreferencesKey("active", false);
    public static PreferencesKey TIMER_VISIT = new PreferencesKey("time_visit", 0L);
    public static PreferencesKey PIN = new PreferencesKey("pin", "");
    public static PreferencesKey ACCESS_TOKEN = new PreferencesKey("access_token", "");

    public static PreferencesKey BASE_URL = new PreferencesKey(FilePreference.UrlInformation, "UrlBase", "");
    public static PreferencesKey MY_COUNTRY_CODE = new PreferencesKey(FilePreference.UrlInformation, "myCountry", "CO");
}
