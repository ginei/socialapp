package com.social.app.managers;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;

import com.social.base.utils.AndroidVersionUtil;
import com.social.app.BuildConfig;
import com.social.app.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;


public class FileManager {

    private static String directoryRoot = "{brand_name}/";
    private static String directoryBase;
    private static String chat = "chat/";
    private static String images = "images/";
    private static String audio = "audio/";
    private static String photos = "photos/";
    private static String samplingPhotos = "sampling/";
    private static String receiptsPhotos = "receipts/";

    public static void generateCacheDirectories(Context context) {
        try {
            String brand_name = context.getString(R.string.app_name).replaceAll("[-+.^:,]", "").trim().toLowerCase();
            brand_name = brand_name.replace("'", "");
            brand_name = brand_name.replace(" ", "");
            directoryRoot = directoryRoot.replace("{brand_name}", brand_name);
            directoryBase = context.getCacheDir().getAbsolutePath() + "/" + directoryRoot;
            File file = new File(directoryBase);
            if (file.mkdir() || file.exists()) {
                File fileChat = new File(directoryBase + chat);
                if (fileChat.mkdir() || fileChat.exists()) {
                    File fileImages = new File(directoryBase + chat + images);
                    fileImages.mkdir();

                    File fileAudio = new File(directoryBase + chat + audio);
                    fileAudio.mkdir();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void generatePhotosDirectories() {
        try {
            File externalStorageDirectory = Environment.getExternalStorageDirectory();

            if (externalStorageDirectory.exists()) {
                File filePhotos = new File(externalStorageDirectory + "/" + photos);
                if (filePhotos.mkdir() || filePhotos.exists()) {
                    File fileSampling = new File(externalStorageDirectory + "/" + photos + samplingPhotos);
                    fileSampling.mkdir();

                    File fileReceipts = new File(externalStorageDirectory + "/" + photos + receiptsPhotos);
                    fileReceipts.mkdir();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getImagesChatPath(String orderId) {
        return FileManager.directoryBase + FileManager.chat + FileManager.images + orderId + "/";
    }

    public static String getAudioChatPath() {
        return FileManager.directoryBase + FileManager.chat + FileManager.audio;
    }

    public static String getSamplePhotosPath() {
        return FileManager.photos + FileManager.samplingPhotos;
    }

    public static String getReceiptsPhotosPath() {
        return FileManager.photos + FileManager.receiptsPhotos;
    }

    public static void saveImage(Bitmap source, String nameImage, String orderId) {
        try {
            File file = new File(getImagesChatPath(orderId));
            if (!file.exists())
                file.mkdir();
            File imageFile = new File(file.getAbsoluteFile() + "/" + nameImage);
            saveImageChat(imageFile, source);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveImageChat(File file, Bitmap bitmap) {
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            int maxSize = 700;
            float ratio;
            int newWidth;
            int newHeight;
            if (bitmap.getWidth() > bitmap.getHeight()) {
                ratio = (float) maxSize / (float) bitmap.getWidth();
                newWidth = maxSize;
                newHeight = (int) (ratio * bitmap.getHeight());
            } else {
                ratio = (float) maxSize / (float) bitmap.getHeight();
                newWidth = (int) (ratio * bitmap.getWidth());
                newHeight = maxSize;
            }
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
            resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            FileOutputStream fo = new FileOutputStream(file);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteImageFolder(String orderId) {
        File file = new File(getImagesChatPath(orderId));
        return file.exists() && file.delete();
    }

    public static Uri generateUriForPicture(Context context, String folderPath) {
        String name = String.valueOf(System.currentTimeMillis());
        File folder = new File(Environment.getExternalStorageDirectory() + "/" + folderPath);
        if (!folder.exists()) {
            folder.mkdir();
        }
        File file = new File(folder, name + ".jpg");

        return FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file);
    }

    public static File getLastModifiedFileOnExternalStorageInFolder(String folder) {
        File file = null;
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        File folderFile = new File(externalStorageDirectory + "/" + folder);
        if (folderFile.exists() && folderFile.isDirectory()) {
            File[] files = folderFile.listFiles(File::isFile);
            long lastMod = Long.MIN_VALUE;
            if (files != null) {
                for (File fileToCheck : files) {
                    if (fileToCheck.lastModified() > lastMod) {
                        file = fileToCheck;
                        lastMod = fileToCheck.lastModified();
                    }
                }
            }
        }
        return file;
    }


    @NonNull
    public static Intent getCapturePhotoIntent(Context context, Uri uri) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        if (!AndroidVersionUtil.INSTANCE.hasLollipop()) {
            List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            }
        }
        return intent;
    }

}
