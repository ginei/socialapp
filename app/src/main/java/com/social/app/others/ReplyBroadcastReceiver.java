package com.social.app.others;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.app.RemoteInput;

import com.social.app.services.pushNotifications.PushNotificationBuilder;


public class ReplyBroadcastReceiver extends BaseBroadcastReceiver {

    public static final String KEY_TEXT_REPLY = "key_text_reply";

    public ReplyBroadcastReceiver() {
        getComponent().inject(this);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // Get remote input from received intent. Remote input is an answer a user has added to the reply. In our case it should contain a String.
        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
        if (remoteInput != null) {
            int notificationId = intent.getIntExtra(PushNotificationBuilder.Companion.getNOTIFICATION_ID(), 0);
        }
    }
}