package com.social.app.others;

import android.content.BroadcastReceiver;

import com.social.app.di.component.BroadcastComponent;
import com.social.app.di.component.DaggerBroadcastComponent;
import com.social.app.ui.GenericApplication;

public abstract class BaseBroadcastReceiver extends BroadcastReceiver {

    protected BroadcastComponent getComponent() {
        return DaggerBroadcastComponent.builder()
                .appComponent(GenericApplication.get().getAppComponent())
                .build();
    }
}
