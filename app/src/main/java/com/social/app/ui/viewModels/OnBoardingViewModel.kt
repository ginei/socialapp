package com.social.app.ui.viewModels

import androidx.databinding.BaseObservable
import android.graphics.drawable.Drawable

class OnBoardingViewModel : BaseObservable() {

    var intDrawable: Int? = null
    var drawable: Drawable? = null
    var title: String? = null
    var message: String? = null

}