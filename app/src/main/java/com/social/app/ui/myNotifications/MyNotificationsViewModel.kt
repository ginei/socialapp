package com.social.app.ui.myNotifications

import androidx.databinding.ObservableBoolean
import com.social.app.api.controllers.GeocodingController
import com.social.app.api.controllers.RastreoApiController
import com.social.app.api.models.alarms.NotificationsModel
import com.social.app.providers.AlertProvider
import com.social.app.ui.viewModels.BaseViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.util.*
import javax.inject.Inject

class MyNotificationsViewModel : BaseViewModel() {

    @Inject
    lateinit var rastreoApiController: RastreoApiController

    @Inject
    lateinit var geocodingController: GeocodingController

    @Inject
    lateinit var alertProvider: AlertProvider

    var refreshing = ObservableBoolean()

    var withoutNotification = ObservableBoolean()

    private val notificationsItem = BehaviorSubject.createDefault(Collections.emptyList<NotificationsModel>())

    private val deleteItemSubject = PublishSubject.create<NotificationsModel>()

    init {
        component.inject(this)
    }

    fun subscribe() {
        disposables.add(alertProvider.onAlertListChange().subscribe({ updateNotificationView() }))
    }

    private fun updateNotificationView() {
        val list: List<NotificationsModel> = alertProvider.getMyAlertList()
        withoutNotification.set(list.isEmpty())
        notificationsItem.onNext(list)
    }

    fun getNotifications() {
        refreshing.set(true)
        disposables.add(rastreoApiController.notifications().subscribe(
                {
                    alertProvider.updateAlertList(it)
                    refreshing.set(false)
                }, { refreshing.set(false);showServiceError(it) }))
    }

    fun deleteNotification(notificationsModel: NotificationsModel) {
        disposables.add(notificationsModel.id.let {
            showLoading()
            rastreoApiController.deleteNotification(it).subscribe({
                hideLoading()
                deleteItemSubject.onNext(notificationsModel)
            }, { showServiceError(it) })
        })
    }

    fun observableNotificationsItems(): Observable<List<NotificationsModel>> =
            notificationsItem.observeOn(AndroidSchedulers.mainThread())

    fun getDeleteItemSubject(): Observable<NotificationsModel> =
            deleteItemSubject.observeOn(AndroidSchedulers.mainThread())

}