package com.social.app.ui.views;


import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.social.base.ui.adapters.GenericAdapter;
import com.social.base.ui.interfaces.GenericItemView;
import com.social.base.utils.ImageUtil;
import com.social.app.R;
import com.social.app.databinding.ViewOptionProfileBinding;
import com.social.app.ui.genericModel.OptionItem;


public class OptionProfileView extends LinearLayout implements GenericItemView<OptionItem> {

    private OptionItem optionItem;

    private ViewOptionProfileBinding binding;

    public OptionProfileView(Context context) {
        super(context);
        init();
    }

    private void init() {
        binding = ViewOptionProfileBinding.inflate(LayoutInflater.from(getContext()), this, true);

        TypedValue typedValue = new TypedValue();
        getContext().getTheme().resolveAttribute(R.attr.selectableItemBackground, typedValue, true);

        setBackgroundResource(typedValue.resourceId);
    }

    @Override
    public void bind(OptionItem item) {
        this.optionItem = item;
        binding.textView.setText(item.getText());
        int drawableId = item.getDrawable();
        if (drawableId != 0) {
            binding.imageView.setImageDrawable(ImageUtil.getDrawableVector(getContext(), item.getDrawable()));
            binding.imageView.setVisibility(View.VISIBLE);
        } else {
            binding.imageView.setVisibility(View.GONE);
        }
        setTag(item.getTag());
    }

    @Override
    public OptionItem getData() {
        return optionItem;
    }

    @Override
    public void setItemClickListener(GenericAdapter.OnItemClickListener onItemClickListener) {

    }

    @Override
    public int getIdForClick() {
        return 0;
    }
}
