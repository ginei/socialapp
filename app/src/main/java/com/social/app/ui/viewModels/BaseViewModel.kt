package com.social.app.ui.viewModels

import android.util.Pair
import androidx.annotation.StringRes
import androidx.databinding.BaseObservable
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.google.android.material.snackbar.Snackbar
import com.social.base.ui.factories.SnackBarFactory
import com.social.base.utils.ErrorUtil
import com.social.app.di.component.DaggerViewModelComponent
import com.social.app.di.component.ViewModelComponent
import com.social.app.ui.GenericApplication
import com.social.app.ui.events.SnackBarEvent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject

open class BaseViewModel : BaseObservable(), LifecycleObserver {

    protected val disposables: CompositeDisposable by lazy {
        CompositeDisposable()
    }

    private val showLoading = BehaviorSubject.create<Boolean>()

    private val showKeyboard = PublishSubject.create<Boolean>()

    private val snackBarSubject = PublishSubject.create<SnackBarEvent>()

    private val showProgressDialog = BehaviorSubject.createDefault(Pair(false, 0))

    protected val component: ViewModelComponent
        get() = DaggerViewModelComponent.builder()
                .appComponent(application.getAppComponent())
                .build()

    protected val application: GenericApplication
        get() = GenericApplication.get()

    protected open fun subscribeToObservables() {}

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    open fun onStart() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    open fun onStop() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    open fun onResume() {
        subscribeToObservables()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    open fun onPause() {
        disposables.clear()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    open fun onDestroy() {
        disposables.dispose()
    }

    fun showLoading() {
        showLoading.onNext(true)
    }

    fun hideLoading() {
        showLoading.onNext(false)
    }

    protected fun hideProgressDialog() {
        showProgressDialog.onNext(Pair<Boolean, Int>(false, null))
    }

    fun showServiceError(throwable: Throwable) {
        showSnackBarMessage(SnackBarFactory.TYPE_ERROR, ErrorUtil.getMessageError(throwable), Snackbar.LENGTH_LONG)
        hideLoading()
        hideProgressDialog()
    }

    protected fun showSnackBarError(message: String) {
        showSnackBarMessage(SnackBarFactory.TYPE_ERROR, message, Snackbar.LENGTH_LONG)
    }

    protected fun showSnackBarError(messageId: Int) {
        showSnackBarMessage(SnackBarFactory.TYPE_ERROR, messageId, Snackbar.LENGTH_LONG)
    }

    protected fun showSnackBarMessage(@SnackBarFactory.SnackBarType typeSnackBar: String, stringResId: Int, duration: Int) {
        val message = GenericApplication.get().resources.getString(stringResId)
        showSnackBarMessage(typeSnackBar, message, duration)
    }

    protected fun showSnackBarMessage(@SnackBarFactory.SnackBarType typeSnackBar: String, message: String?, duration: Int) {
        snackBarSubject.onNext(SnackBarEvent(typeSnackBar, message, duration))
    }

    fun observableShowLoading(): Observable<Boolean> {
        return showLoading.observeOn(AndroidSchedulers.mainThread())
    }

    fun observableShowProgress(): Observable<Pair<Boolean, Int>> {
        return showProgressDialog.observeOn(AndroidSchedulers.mainThread())
    }

    fun observableShowKeyboard(): Observable<Boolean> {
        return showKeyboard.observeOn(AndroidSchedulers.mainThread())
    }

    fun observableSnackBar(): Observable<SnackBarEvent> {
        return snackBarSubject.observeOn(AndroidSchedulers.mainThread())
    }

}
