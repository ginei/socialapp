package com.social.app.ui;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.social.app.BuildConfig;
import com.social.app.R;
import com.social.app.di.component.AppComponent;
import com.social.app.di.component.DaggerAppComponent;
import com.social.app.di.modules.AppModule;
import com.social.base.utils.FileUtil;

import io.fabric.sdk.android.Fabric;

public class GenericApplication extends MultiDexApplication {

    private static GenericApplication sInstance;

    public AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        updateDagger();
        initialize();
        Fabric.with(this, new Crashlytics());
    }

    private void initialize() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        FileUtil.init(BuildConfig.APPLICATION_ID);
        FileUtil.generateBasicFolders(getString(R.string.app_name));
    }

    public static GenericApplication get() {
        return sInstance;
    }

    public void updateDagger() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

}
