package com.social.app.ui.fragments;

import android.app.Activity;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.cursoradapter.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.social.app.R;
import com.social.app.managers.database.Database;
import com.social.app.managers.database.Devices;
import com.social.app.managers.database.SQLiteCursorLoader;


public class DevicesFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String TAG = DevicesFragment.class.toString();

    private OnDevicesListener presenter;
    private DevicesCursorAdapter mAdapter;
    private CoordinatorLayout coordinatorLayout;

    public static DevicesFragment instance() {
        return new DevicesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getLoaderManager().initLoader(0, null, this);
        final View view = inflater.inflate(R.layout.fragment_devices, container, false);
        this.coordinatorLayout = view.findViewById(R.id.coordinatorLayout);
        final FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(view1 -> presenter.onScanStart());
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnDevicesListener) {
            this.presenter = (OnDevicesListener) activity;
        } else {
            throw new ClassCastException("must implement OnDevicesListener");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAdapter = new DevicesCursorAdapter(getActivity());
        setListAdapter(mAdapter);
        getListView().setOnItemClickListener((adapterView, view, position, l) -> {
            final TextView name = view.findViewById(android.R.id.text1);
            final TextView address = view.findViewById(android.R.id.text2);
            presenter.onDevice(name.getText().toString(), address.getText().toString());
        });
        getListView().setOnItemLongClickListener((adapterView, view, i, l) -> {
            final TextView name = view.findViewById(android.R.id.text1);
            final TextView address = view.findViewById(android.R.id.text2);
            final CheckBox enabled = view.findViewById(android.R.id.selectedIcon);
            presenter.onChangeDeviceName(name.getText().toString(), address.getText().toString(), enabled.isChecked());
            return true;
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        this.presenter.onDevicesStarted();
    }

    @Override
    public void onStop() {
        super.onStop();
        this.presenter.onDevicesStopped();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new SQLiteCursorLoader(
                getActivity(),
                Database.getDatabaseHelperInstance(getActivity()),
                Devices.SELECT_DEVICES,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    public void refresh() {
        getLoaderManager().restartLoader(0, null, this);
    }

    public void snack(String message) {
        Snackbar.make(this.coordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    public interface OnDevicesListener {

        void onDevicesStarted();

        void onDevicesStopped();

        void onDevice(String name, String address);

        void onChangeDeviceName(String name, String address, boolean checked);

        void onDeviceStateChanged(String address, boolean enabled);

        void onScanStart();
    }

    class DevicesCursorAdapter extends SimpleCursorAdapter {

        public DevicesCursorAdapter(Context context) {
            super(context,
                    R.layout.expandable_list_item_with_options, null,
                    new String[]{
                            Devices.NAME,
                            Devices.ADDRESS,
                            Devices.ENABLED
                    },
                    new int[]{
                            android.R.id.text1,
                            android.R.id.text2
                    }, 0);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            super.bindView(view, context, cursor);
            final TextView address = view.findViewById(android.R.id.text2);
            final CheckBox button = view.findViewById(android.R.id.selectedIcon);

            final String device = address.getText().toString();
            final int column = cursor.getColumnIndex(Devices.ENABLED);
            final boolean enabled = cursor.getInt(column) == 1;

            Log.d(TAG, "device: " + device + " enabled: " + enabled);

            button.setChecked(enabled);
            button.setOnClickListener(view1 -> {
                final boolean b = button.isChecked();
                Log.d(TAG, "onClick() device: " + device + " enabled: " + b);
                presenter.onDeviceStateChanged(device, b);
            });
        }
    }
}
