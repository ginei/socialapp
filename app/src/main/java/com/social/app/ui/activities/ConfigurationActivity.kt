package com.social.app.ui.activities

import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.*
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.social.app.R
import com.social.app.databinding.ActivityConfigurationBinding
import com.social.app.managers.database.Devices
import com.social.app.services.BluetoothLEService
import com.social.app.ui.BaseActivity
import com.social.app.ui.fragments.DevicesFragment
import com.social.app.utils.ServiceUtils
import java.util.*

class ConfigurationActivity : BaseActivity(), DevicesFragment.OnDevicesListener {

    override fun onDevice(name: String?, address: String?) {
        bindService(Intent(this, BluetoothLEService::class.java), serviceConnection, BIND_AUTO_CREATE)
    }

    override fun onChangeDeviceName(name: String?, address: String?, checked: Boolean) {
//        DeviceAlertDialogFragment.instance(name, address, checked).show(fragmentManager, "dialog")
    }

    override fun onDeviceStateChanged(address: String?, enabled: Boolean) {

        service?.let {
            Devices.setEnabled(this, address, enabled)

            if (enabled) {
                it.connect(address)
            } else {
//                AlertDialogFragment.instance(R.string.app_name, R.string.link_loss_disabled).show(fragmentManager, null)
                it.disconnect(address)
            }

        }
    }

    private val REQUEST_ENABLE_BT = 1
    private val SCAN_PERIOD: Long = 10000 // 10 seconds

    private val binding: ActivityConfigurationBinding by lazy {
        DataBindingUtil.setContentView<ActivityConfigurationBinding>(this, R.layout.activity_configuration)
    }
    private var service: BluetoothLEService? = null

    private var stopScan: Runnable? = null

    private var bluetoothAdapter: BluetoothAdapter? = null

    private val devices = HashMap<String, String>()
    private val mHandler: Handler by lazy {
        Handler()
    }

    private val devicesFragment = DevicesFragment.instance()

    private val mLeScanCallback = BluetoothAdapter.LeScanCallback { device, _, _ ->
        val address = device.address
        val name = device.name
        // detect Bluetooth LE support
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show()
            finish()
        }

        Log.e("Configuration", "device $name with address $address found")
        if (!Devices.containsDevice(this@ConfigurationActivity, address)) {
            devices.put(name ?: address, address)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBar(binding.toolbar)

        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager.adapter

        showDevices()

        if (!ServiceUtils.isServiceRunning((BluetoothLEService::class.java))) {
            startService(Intent(this, BluetoothLEService::class.java))
        }
    }

    private fun showDevices() {
        fragmentManager.beginTransaction().replace(R.id.container, devicesFragment).commit()
    }

    override fun onScanStart() {
        devices.clear()
        stopScan = Runnable {
            mHandler.removeCallbacks(stopScan)
            bluetoothAdapter?.stopLeScan(mLeScanCallback)
            showLoading(false)
            if (!devices.isEmpty()) {
                displayListScannedDevices()
            } else {
                showError(binding.root, getString(R.string.beacon_not_found))
            }
        }
        mHandler.postDelayed(stopScan, SCAN_PERIOD)
        bluetoothAdapter?.startLeScan(mLeScanCallback)
        showLoading(true)
    }

    private fun displayListScannedDevices() {
        val builderSingle = AlertDialog.Builder(this)
        builderSingle.setIcon(R.drawable.logo_social)
        builderSingle.setTitle(R.string.select_device)
        val arrayAdapter = ArrayAdapter<String>(baseContext, android.R.layout.select_dialog_singlechoice)
        arrayAdapter.addAll(devices.keys)
        builderSingle.setSingleChoiceItems(arrayAdapter, 0) { dialog, witch ->
            val device = arrayAdapter.getItem(witch)
            devices[device]?.let { selectDevice(device, it) }
            dialog.dismiss()
        }
        builderSingle.setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.cancel() }
        builderSingle.show()
    }

    private fun selectDevice(name: String, address: String) {
        Devices.insert(this, name, address)
        Devices.setEnabled(this, address, true)
        service?.connect(address)
        devicesFragment.refresh()
    }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            if (iBinder is BluetoothLEService.BackgroundBluetoothLEBinder) {
                service = iBinder.service()
            }
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            Log.d(BluetoothLEService.TAG, "onServiceDisconnected()")
        }
    }

    private fun subscribe() {}


    override fun onDevicesStarted() {
        // bind service
        bindService(Intent(this, BluetoothLEService::class.java), serviceConnection, BIND_AUTO_CREATE)
    }

    override fun onDevicesStopped() {
        bluetoothAdapter?.stopLeScan(mLeScanCallback)
        mHandler.removeCallbacks(stopScan)

        unbindService(serviceConnection)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        subscribe()
        super.onResume()
    }

    override fun onStart() {
        super.onStart()

        if (!isBluetoothEnabled()) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        } else {
            onScanStart()
        }
    }

    private fun isBluetoothEnabled(): Boolean {
        bluetoothAdapter?.let {
            return it.isEnabled
        } ?: return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_ENABLE_BT) {
            val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            bluetoothAdapter = bluetoothManager.adapter
            if (!isBluetoothEnabled()) {
                finish()
            } else {
                onScanStart()
            }
        }

    }

}