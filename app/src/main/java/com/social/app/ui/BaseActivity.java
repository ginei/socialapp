package com.social.app.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;

import com.social.base.eventbus.RxBus;
import com.social.base.preferences.PrefsManager;
import com.social.app.di.component.ActivityComponent;
import com.social.app.di.component.DaggerActivityComponent;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;


public class BaseActivity extends com.social.base.ui.BaseActivity {

    @Inject
    RxBus rxBus;

    @Inject
    PrefsManager prefsManager;

    protected CompositeDisposable subscriptions;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        subscriptions = new CompositeDisposable();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public ActivityComponent getComponent() {
        return DaggerActivityComponent.builder()
                .appComponent(GenericApplication.get().getAppComponent())
                .build();
    }

    public void setupActionBar(Toolbar toolbar) {
        setupActionBar(toolbar, true);
    }

    protected void setupActionBar(Toolbar toolbar, boolean showButtonBack) {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(showButtonBack);
            actionBar.setDisplayShowHomeEnabled(showButtonBack);
        }
    }

    @Override
    public void onPause() {
        subscriptions.clear();
        super.onPause();
    }

}
