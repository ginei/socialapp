package com.social.app.ui.events;


import com.social.base.ui.factories.SnackBarFactory;

public class SnackBarEvent {

    private @SnackBarFactory.SnackBarType String typeSnackBar;
    private String message;

    public SnackBarEvent(String typeSnackBar, String message, int duration) {
        this.typeSnackBar = typeSnackBar;
        this.message = message;
    }

    public @SnackBarFactory.SnackBarType String getTypeSnackBar() {
        return typeSnackBar;
    }

    public String getMessage() {
        return message;
    }
}
