package com.social.app.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.facebook.accountkit.AccountKitLoginResult
import com.facebook.accountkit.PhoneNumber
import com.facebook.accountkit.ui.AccountKitActivity
import com.facebook.accountkit.ui.AccountKitConfiguration
import com.facebook.accountkit.ui.LoginType
import com.social.app.R
import com.social.app.databinding.ActivityLoginBinding
import com.social.app.ui.BaseActivity
import com.social.app.ui.activities.MainActivity

private const val APP_REQUEST_CODE = 100

class LoginActivity : BaseActivity() {

    val binding: ActivityLoginBinding by lazy {
        DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
    }

    val viewModel: LoginViewModel by lazy {
        LoginViewModel()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel
    }

    private fun subscribe() {
        subscriptions.add(viewModel.actionNextObservable().subscribe { openAccountKit() })
        subscriptions.add(viewModel.actionSuccessLoginObservable().subscribe { startMainActivity() })

        subscriptions.add(viewModel.observableSnackBar().subscribe { showMessage(it.typeSnackBar, binding.root, it.message) })
        subscriptions.add(viewModel.observableShowProgress()
                .subscribe { showProgressDialog(it) })

        subscriptions.add(viewModel.observableShowLoading()
                .subscribe { showLoading(it) })
    }

    private fun startMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    private fun openAccountKit() {
        val intent = Intent(this, AccountKitActivity::class.java)

        val configurationBuilder = AccountKitConfiguration.AccountKitConfigurationBuilder(
                LoginType.PHONE, AccountKitActivity.ResponseType.TOKEN)
                .setInitialPhoneNumber(PhoneNumber("", "", "CO"))
                .setReceiveSMS(true)

        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build())

        startActivityForResult(intent, APP_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        when (requestCode) {
            APP_REQUEST_CODE ->
                if (resultCode == RESULT_OK) {
                    val loginResult = data?.getParcelableExtra<AccountKitLoginResult>(AccountKitLoginResult.RESULT_KEY)
                    loginResult?.let { viewModel.handleAccountKitResult(it) }
                }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onResume() {
        super.onResume()
        subscribe()
    }

}