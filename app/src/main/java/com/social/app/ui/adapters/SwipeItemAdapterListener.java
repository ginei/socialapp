package com.social.app.ui.adapters;


import com.social.app.ui.views.CustomSwipeLayout;

public class SwipeItemAdapterListener implements CustomSwipeLayout.SwipeListener {


    @Override
    public void onStartOpen(CustomSwipeLayout layout) {

    }

    @Override
    public void onOpen(CustomSwipeLayout layout) {

    }

    @Override
    public void onStartClose(CustomSwipeLayout layout) {

    }

    @Override
    public void onClose(CustomSwipeLayout layout) {

    }

    @Override
    public void onUpdate(CustomSwipeLayout layout, int leftOffset, int topOffset) {

    }

    @Override
    public void onHandRelease(CustomSwipeLayout layout, float xvel, float yvel) {

    }
}
