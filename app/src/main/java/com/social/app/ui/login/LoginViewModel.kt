package com.social.app.ui.login

import com.facebook.accountkit.*
import com.social.base.preferences.PrefsManager
import com.social.app.api.controllers.RastreoApiController
import com.social.app.api.models.ProfileResponse
import com.social.app.ui.viewModels.BaseViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class LoginViewModel : BaseViewModel() {

    @Inject
    lateinit var rastreoApiController: RastreoApiController

    @Inject
    lateinit var prefsManager: PrefsManager

    private var actionNext = PublishSubject.create<Unit>()
    private var registerNext = PublishSubject.create<Unit>()
    private var successLogin = PublishSubject.create<Unit>()

    init {
        component.inject(this)
    }

    fun onLoginClicked() {
        actionNext.onNext(Unit)
    }

    fun onRegisterClicked() {
        registerNext.onNext(Unit)
    }

    fun handleAccountKitResult(accountKitLoginResult: AccountKitLoginResult) {

        val accessToken = accountKitLoginResult.accessToken

        if (accessToken != null) {

            AccountKit.getCurrentAccount(object : AccountKitCallback<Account> {
                override fun onSuccess(account: Account) {
                    val accountKitPhone = account.phoneNumber
                    val phoneFromAccountKit = accountKitPhone.rawPhoneNumber.replaceFirst(accountKitPhone.countryCode.toRegex(), "")
                    verifyUserRegister(phoneFromAccountKit)
                }

                override fun onError(error: AccountKitError) {
                    showSnackBarError(error.userFacingMessage)
                }
            })
        }
    }

    fun verifyUserRegister(phoneFromAccountKit: String) {
        rastreoApiController.isUserRegistered(phoneFromAccountKit)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    showLoading()
                }.subscribe({
                    hideLoading()
                    val profileResponse = ProfileResponse()
                    profileResponse.phone = phoneFromAccountKit
                    successLogin.onNext(Unit)
                }, { showServiceError(it) })
    }

    fun actionNextObservable(): Observable<Unit> =
            actionNext.observeOn(AndroidSchedulers.mainThread())

    fun actionRegisterObservable(): Observable<Unit> =
            registerNext.observeOn(AndroidSchedulers.mainThread())

    fun actionSuccessLoginObservable(): Observable<Unit> =
            successLogin.observeOn(AndroidSchedulers.mainThread())

}