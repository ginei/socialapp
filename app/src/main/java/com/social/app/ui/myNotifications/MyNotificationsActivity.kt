package com.social.app.ui.myNotifications

import android.os.Bundle
import android.view.MenuItem
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.social.base.ui.adapters.GenericAdapter
import com.social.base.ui.factories.GenericAdapterFactory
import com.social.base.ui.interfaces.GenericItem
import com.social.base.ui.interfaces.GenericItemAbstract
import com.social.base.ui.interfaces.GenericItemView
import com.social.app.R
import com.social.app.api.models.alarms.NotificationsModel
import com.social.app.databinding.ActivityMyNotificationsBinding
import com.social.app.ui.BaseActivity
import com.social.app.ui.itemsView.ItemNotificationsView

class MyNotificationsActivity : BaseActivity(), ItemNotificationsView.SwipeNotificationListener {

    private val binding: ActivityMyNotificationsBinding by lazy {
        DataBindingUtil.setContentView<ActivityMyNotificationsBinding>(this, R.layout.activity_my_notifications)
    }

    private val viewModel: MyNotificationsViewModel by lazy {
        MyNotificationsViewModel()
    }

    private val adapter: GenericAdapter  by lazy {
        GenericAdapter(object : GenericAdapterFactory() {

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericItemView<*> {
                return ItemNotificationsView(parent.context, true, this@MyNotificationsActivity)
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel
        setupActionBar(binding.toolbar)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        binding.recyclerViewNotifications.apply {
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            setItemViewCacheSize(10)
            adapter = this@MyNotificationsActivity.adapter
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun subscribe() {
        subscriptions.add(viewModel.observableSnackBar().subscribe { snackBarEvent -> showError(binding.root, snackBarEvent.message) })
        subscriptions.add(viewModel.observableShowLoading().subscribe { showLoading(it) })
        subscriptions.add(viewModel.observableNotificationsItems().subscribe { loadItemsAlarm(it) })

        subscriptions.add(viewModel.getDeleteItemSubject().subscribe { deleteNotification(it) })

        viewModel.subscribe()
        viewModel.getNotifications()
    }

    private fun loadItemsAlarm(items: List<NotificationsModel>) {
        adapter.clearItems()
        val listItems = ArrayList<GenericItem<*>>()
        items.forEach { listItems.add(GenericItemAbstract(it)) }
        adapter.addItems(listItems)
    }

    override fun onResume() {
        super.onResume()
        subscribe()
    }

    override fun onSwipeOrder(itemNotificationsView: ItemNotificationsView) {

    }

    override fun onClickedDelete(itemNotificationsView: ItemNotificationsView) {
        itemNotificationsView.data.id.let { viewModel.deleteNotification(itemNotificationsView.data) }
    }

    private fun deleteNotification(notificationsModel: NotificationsModel) {
        for (i in 0 until adapter.itemCount) {
            val itemModel = adapter.getItemAtPosition(i)
            if (itemModel.data is NotificationsModel) {
                val current = itemModel.data as NotificationsModel
                if (notificationsModel.id == current.id) {
                    adapter.removeItemAtPosition(i)
                    break
                }
            }
        }
    }

}