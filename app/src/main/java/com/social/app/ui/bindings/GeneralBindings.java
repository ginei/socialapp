package com.social.app.ui.bindings;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import androidx.annotation.DrawableRes;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.viewpager.widget.ViewPager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.social.base.utils.ImageUtil;
import com.social.app.R;
import com.social.app.ui.GenericApplication;
import com.social.app.ui.adapters.CustomPagerAdapter;
import com.social.app.ui.viewModels.OnBoardingViewModel;

import java.io.File;
import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

public class GeneralBindings {

    @BindingAdapter("load_profile_image")
    public static void setProfilePhoto(ImageView imageView, String imagePath) {
        Context context = GenericApplication.get().getApplicationContext();
        RequestOptions options = new RequestOptions();
        options.centerCrop();
        options.placeholder(R.drawable.logo_social);
        options.error(R.drawable.logo_social);

        Glide.with(context)
                .asBitmap()
                .load(imagePath)
                .apply(options)
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        imageView.setImageDrawable(circularBitmapDrawable);
                    }
                });
    }

    @BindingAdapter(value = {"load_image", "placeholder"}, requireAll = false)
    public static void loadImage(ImageView imageView, String imageUrl, Drawable placeholder) {
        if (imageUrl != null) {

            RequestOptions options = new RequestOptions()
                    .placeholder(placeholder)
                    .dontAnimate();

            Glide.with(imageView.getContext())
                    .load(imageUrl)
                    .apply(options)
                    .into(imageView);

        } else if (placeholder != null) {
            imageView.setImageDrawable(placeholder);
        }
    }

    @BindingAdapter(value = {"load_image", "placeholder", "load_svg"}, requireAll = false)
    public static void loadImage(ImageView imageView, String imageUrl, Drawable placeholder, @DrawableRes int svg) {
        if (svg != 0) {
            loadSvg(imageView, svg);
        } else {
            loadImage(imageView, imageUrl, placeholder);
        }
    }

    @BindingAdapter("load_svg")
    public static void loadSvg(ImageView imageView, @DrawableRes int drawableRes) {
        if (drawableRes != 0) {
            imageView.setImageDrawable(ImageUtil.getDrawableVector(imageView.getContext(), drawableRes));
        }
    }

    @BindingAdapter(value = {"circleIndicator", "images", "titles", "messages"}, requireAll = false)
    public static void setPagerAdapter(ViewPager viewPager, CircleIndicator circleIndicator, TypedArray images, String[] titles, String[] messages) {

        if (images != null && titles != null && messages != null) {
            ArrayList<OnBoardingViewModel> items = new ArrayList<>(titles.length);
            for (int i = 0; i < titles.length; i++) {
                OnBoardingViewModel onBoardingViewModel = new OnBoardingViewModel();
                if (i < images.length()) {
                    onBoardingViewModel.setDrawable(images.getDrawable(i));
                }
                if (i < titles.length) {
                    onBoardingViewModel.setTitle(titles[i]);
                }
                if (i < messages.length) {
                    onBoardingViewModel.setMessage(messages[i]);
                }
                items.add(onBoardingViewModel);
            }
            CustomPagerAdapter customPagerAdapter = new CustomPagerAdapter(viewPager.getContext());
            customPagerAdapter.setItems(items);
            viewPager.setAdapter(customPagerAdapter);
            circleIndicator.setViewPager(viewPager);
        }
    }

    @BindingAdapter("load_image_file")
    public static void loadImageFile(ImageView imageView, File fileImage) {
        if (fileImage != null) {
            Glide.with(imageView.getContext()).load(fileImage).into(imageView);
        }
    }
}
