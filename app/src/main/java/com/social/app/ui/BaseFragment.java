package com.social.app.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;


import com.social.base.utils.AndroidVersionUtil;
import com.social.app.R;
import com.social.app.di.component.DaggerFragmentComponent;
import com.social.app.di.component.FragmentComponent;
import io.reactivex.disposables.CompositeDisposable;

public class BaseFragment extends com.social.base.ui.BaseFragment {

    protected CompositeDisposable subscriptions;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        subscriptions = new CompositeDisposable();
    }

    public FragmentComponent getComponent() {
        return DaggerFragmentComponent.builder()
                .appComponent(GenericApplication.get().getAppComponent())
                .build();
    }

    public int getContainer() {
        return R.id.container;
    }

    @Override
    public void onPause() {
        subscriptions.clear();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void changeStatusBarColor(int color) {
        if (AndroidVersionUtil.INSTANCE.hasLollipop()) {
            getActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), color));
        }
    }

}
