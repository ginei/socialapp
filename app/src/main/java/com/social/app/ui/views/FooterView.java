package com.social.app.ui.views;


import android.content.Context;
import androidx.annotation.AttrRes;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.social.app.R;
import com.social.app.databinding.ViewFooterBinding;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class FooterView extends FrameLayout {

    public static final int INIT = 1;
    public static final int MENU = 3;

    private ViewFooterBinding binding;

    private FooterItemView footerItemViewSelected;
    private OnClickListener onClickListener;

    private FooterItemView itemViewInit;
    private FooterItemView itemViewConfiguration;

    public FooterView(Context context) {
        super(context);
        init();
    }

    public FooterView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FooterView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setBackgroundResource(R.color.white);
        binding = ViewFooterBinding.inflate(LayoutInflater.from(getContext()), this, true);
        initViews();
        setupListeners();
        selectInitialItem(INIT);
    }

    private void setupListeners() {
        itemViewInit.setOnClickListener(this::onFooterItemClicked);
        itemViewConfiguration.setOnClickListener(this::onFooterItemClicked);
    }

    private void initViews() {
        itemViewInit = binding.footerItemViewInit;
        itemViewConfiguration = binding.footerItemViewMenu;

        itemViewInit.setTag(INIT);
        itemViewConfiguration.setTag(MENU);
    }

    public void selectInitialItem(@itemFooter int itemSelected) {
        selectInitialItem(itemSelected, true);
    }

    public void selectInitialItem(@itemFooter int itemSelected, boolean actionListener) {
        View itemToSelect;
        switch (itemSelected) {
            case INIT:
                itemToSelect = binding.footerItemViewInit;
                break;
            case MENU:
                itemToSelect = binding.footerItemViewMenu;
                break;
            default:
                itemToSelect = binding.footerItemViewInit;
        }
        onFooterItemClicked(itemToSelect, actionListener);
    }

    private void onFooterItemClicked(View view) {
        onFooterItemClicked(view, true);
    }

    private void onFooterItemClicked(View view, boolean actionListener) {
        if (footerItemViewSelected != null) {
            footerItemViewSelected.setSelected(false);
        }
        footerItemViewSelected = (FooterItemView) view;
        footerItemViewSelected.setSelected(true);

        if (onClickListener != null && actionListener) {
            onClickListener.onClick(view);
        }
    }

    @Override
    public void setOnClickListener(OnClickListener onClickListener) {
        super.setOnClickListener(onClickListener);
        this.onClickListener = onClickListener;
    }

    @IntDef({INIT, MENU})
    @Retention(RetentionPolicy.SOURCE)
    public @interface itemFooter {
    }

}