package com.social.app.ui.views;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.social.app.R;
import com.social.app.databinding.ViewCustomToolbarBinding;

public class CustomToolbarView extends FrameLayout {

    private ViewCustomToolbarBinding binding;

    private OnStateChangeListener onStateChangeListener;

    public CustomToolbarView(@NonNull Context context) {
        super(context);
        init();
    }

    public CustomToolbarView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomToolbarView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setBackgroundResource(R.color.blue);
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.view_custom_toolbar, this, true);
        initListeners();
    }

    private void initListeners() {
        binding.switchView.setOnCheckedChangeListener((compoundButton, newState) -> {
            if (onStateChangeListener != null) {
                onStateChangeListener.stateChanged(!newState);
            }
        });
    }

    public void setStateActive(boolean active) {
        int color = active ? R.color.blue : R.color.red_salmon;
        int stateText = active ? R.string.toolbar_active : R.string.toolbar_inactive;
        setBackgroundResource(color);
        binding.textView.setText(stateText);
        binding.switchView.setOnCheckedChangeListener(null);
        binding.switchView.setChecked(active);
        initListeners();
    }

    public void setOnStateChangeListener(OnStateChangeListener onStateChangeListener) {
        this.onStateChangeListener = onStateChangeListener;
    }

    public interface OnStateChangeListener {

        void stateChanged(boolean isCurrentStateActive);
    }
}
