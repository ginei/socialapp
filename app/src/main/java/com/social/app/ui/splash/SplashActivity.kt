package com.social.app.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.social.base.preferences.PrefsManager
import com.social.app.R
import com.social.app.managers.ProfileManager
import com.social.app.managers.preferences.ConstantPreferences
import com.social.app.ui.BaseActivity
import com.social.app.ui.login.LoginActivity
import com.social.app.ui.activities.MainActivity
import com.social.app.ui.pin.PinVerificationActivity
import javax.inject.Inject

private const val LOGIN_PIN = 101

class SplashActivity : BaseActivity() {

    @Inject
    lateinit var prefsManager: PrefsManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
            checkIfUserLogged()
        }, 1500)
    }

    private fun verifyPin() {
        startActivityForResult(PinVerificationActivity.newInstance(this,
                PinVerificationActivity.VERIFY_PIN, getPin()), LOGIN_PIN)
    }

    private fun checkIfUserLogged() {
        if (ProfileManager.isUserLogged) {
            if (getPin().isEmpty()) {
                handleUserLogged()
            } else {
                verifyPin()
            }
        } else {
            goToLogin()
        }
    }

    private fun goToLogin() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private fun handleUserLogged() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            LOGIN_PIN ->
                if (resultCode == BaseActivity.RESULT_OK) {
                    handleUserLogged()
                } else {
                    goToLogin()
                }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun getPin(): String = prefsManager.getString(ConstantPreferences.PIN)
}
