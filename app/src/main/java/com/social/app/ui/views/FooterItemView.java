package com.social.app.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.social.app.R;
import com.social.app.databinding.ViewFooterItemBinding;

public class FooterItemView extends FrameLayout {

    private ViewFooterItemBinding binding;

    private int normalIcon = R.color.colorPrimary;
    private int selectedIcon = R.color.colorPrimary;
    private String message = "";
    private int normalColor = Color.BLACK;
    private int selectedColor = Color.BLACK;

    public FooterItemView(Context context) {
        super(context);
        init();
    }

    public FooterItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(attrs);
        init();
    }

    public FooterItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(attrs);
        init();
    }

    protected void initAttrs(AttributeSet attrs) {

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.footer_item);

        if (typedArray != null) {
            final int N = typedArray.getIndexCount();
            for (int i = 0; i < N; ++i) {
                int attr = typedArray.getIndex(i);
                switch (attr) {
                    case R.styleable.footer_item_normal_icon:
                        normalIcon = typedArray.getResourceId(attr, 0);
                        break;
                    case R.styleable.footer_item_selected_icon:
                        selectedIcon = typedArray.getResourceId(attr, 0);
                        break;
                    case R.styleable.footer_item_message:
                        message = typedArray.getString(attr);
                        break;
                    case R.styleable.footer_item_normal_color:
                        normalColor = typedArray.getColor(attr, Color.BLACK);
                        break;
                    case R.styleable.footer_item_selected_color:
                        selectedColor = typedArray.getColor(attr, Color.WHITE);
                        break;
                }
            }
            typedArray.recycle();
        }

    }

    private void init() {
        binding = ViewFooterItemBinding.inflate(LayoutInflater.from(getContext()), this, false);
        addView(binding.getRoot());

        binding.imageView.setImageResource(normalIcon);
        binding.textView.setText(message);
    }


    public void setSelected(boolean selected) {
        binding.imageView.setImageResource(selected ? selectedIcon : normalIcon);
        binding.textView.setTextColor(selected ? selectedColor : normalColor);
    }

}
