package com.social.app.ui.views;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.core.content.ContextCompat;
import androidx.core.text.TextUtilsCompat;
import androidx.core.view.ViewCompat;
import androidx.appcompat.widget.AppCompatEditText;
import android.text.InputType;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.OvershootInterpolator;

import com.social.app.R;

import java.util.Locale;

public class PinEditText extends AppCompatEditText {

    private static final String XML_NAMESPACE_ANDROID = "http://schemas.android.com/apk/res/android";

    protected String mask = null;
    protected StringBuilder maskChars = null;
    protected String singleCharHint = null;
    protected int animatedType = 0;
    protected float space = 24; //24 dp by default, space between the lines
    protected float charSize;
    protected float numChars = 4;
    protected float textBottomPadding = 8; //8dp by default, height of the text from our lines
    protected int maxLength = 4;
    protected RectF[] lineCoords;
    protected float[] charBottom;
    protected Paint charPaint;
    protected Paint lastCharPaint;
    protected Paint singleCharPaint;
    protected Drawable pinBackground;
    protected Rect textHeight = new Rect();
    protected boolean isDigitSquare = false;

    protected OnClickListener onClickListener;
    protected OnPinEnteredListener onPinEnteredListener = null;

    protected float lineStroke = 1; //1dp by default
    protected float lineStrokeSelected = 2; //2dp by default
    protected Paint linesPaint;
    protected boolean animate = false;
    protected boolean hasError = false;
    protected ColorStateList originalTextColors;
    protected int[][] states = new int[][]{
            new int[]{android.R.attr.state_selected}, // selected
            new int[]{android.R.attr.state_active}, // error
            new int[]{android.R.attr.state_focused}, // focused
            new int[]{-android.R.attr.state_focused}, // unfocused
    };

    protected int[] colors = new int[]{
            Color.GREEN,
            Color.RED,
            Color.BLACK,
            Color.GRAY
    };

    protected ColorStateList colorStateList = new ColorStateList(states, colors);

    public PinEditText(Context context) {
        super(context);
    }

    public PinEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public PinEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        float multi = context.getResources().getDisplayMetrics().density;
        lineStroke = multi * lineStroke;
        lineStrokeSelected = multi * lineStrokeSelected;
        space = multi * space; //convert to pixels for our density
        textBottomPadding = multi * textBottomPadding; //convert to pixels for our density

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.PinEntryEditText, 0, 0);
        try {
            TypedValue outValue = new TypedValue();
            ta.getValue(R.styleable.PinEntryEditText_pinAnimationType, outValue);
            animatedType = outValue.data;
            mask = ta.getString(R.styleable.PinEntryEditText_pinCharacterMask);
            singleCharHint = ta.getString(R.styleable.PinEntryEditText_pinRepeatedHint);
            lineStroke = ta.getDimension(R.styleable.PinEntryEditText_pinLineStroke, lineStroke);
            lineStrokeSelected = ta.getDimension(R.styleable.PinEntryEditText_pinLineStrokeSelected, lineStrokeSelected);
            space = ta.getDimension(R.styleable.PinEntryEditText_pinCharacterSpacing, space);
            textBottomPadding = ta.getDimension(R.styleable.PinEntryEditText_pinTextBottomPadding, textBottomPadding);
            isDigitSquare = ta.getBoolean(R.styleable.PinEntryEditText_pinBackgroundIsSquare, isDigitSquare);
            pinBackground = ta.getDrawable(R.styleable.PinEntryEditText_pinBackgroundDrawable);
            ColorStateList colors = ta.getColorStateList(R.styleable.PinEntryEditText_pinLineColors);
            if (colors != null) {
                colorStateList = colors;
            }
        } finally {
            ta.recycle();
        }

        charPaint = new Paint(getPaint());
        lastCharPaint = new Paint(getPaint());
        singleCharPaint = new Paint(getPaint());
        linesPaint = new Paint(getPaint());
        linesPaint.setStrokeWidth(lineStroke);

        TypedValue outValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorControlActivated,
                outValue, true);
        int colorSelected = outValue.data;
        colors[0] = colorSelected;

        int colorFocused = isInEditMode() ? Color.GRAY : ContextCompat.getColor(context, R.color.colorAccent);
        colors[1] = colorFocused;

        int colorUnfocused = isInEditMode() ? Color.GRAY : ContextCompat.getColor(context, R.color.background);
        colors[2] = colorUnfocused;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setBackground(ContextCompat.getDrawable(context, R.drawable.bg_rounded_pin));
        } else {
            setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.bg_rounded_pin));
        }

        maxLength = attrs.getAttributeIntValue(XML_NAMESPACE_ANDROID, "maxLength", 4);
        numChars = maxLength;

        //Disable copy paste
        super.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });

        //If input type is password and no mask is set, use a default mask
        if ((getInputType() & InputType.TYPE_TEXT_VARIATION_PASSWORD) == InputType.TYPE_TEXT_VARIATION_PASSWORD && TextUtils.isEmpty(mask)) {
            mask = "\u25CF";
        } else if ((getInputType() & InputType.TYPE_NUMBER_VARIATION_PASSWORD) == InputType.TYPE_NUMBER_VARIATION_PASSWORD && TextUtils.isEmpty(mask)) {
            mask = "\u25CF";
        }

        if (!TextUtils.isEmpty(mask)) {
            maskChars = getMaskChars();
        }

        //Height of the characters, used if there is a background drawable
        getPaint().getTextBounds("|", 0, 1, textHeight);

        animate = animatedType > -1;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        originalTextColors = getTextColors();
        if (originalTextColors != null) {
            lastCharPaint.setColor(originalTextColors.getDefaultColor());
            charPaint.setColor(originalTextColors.getDefaultColor());
            singleCharPaint.setColor(getCurrentHintTextColor());
        }
        int availableWidth = getWidth() - ViewCompat.getPaddingEnd(this) - ViewCompat.getPaddingStart(this);
        if (space < 0) {
            charSize = (availableWidth / (numChars * 2 - 1));
        } else {
            charSize = (availableWidth - (space * (numChars - 1))) / numChars;
        }
        lineCoords = new RectF[(int) numChars];
        charBottom = new float[(int) numChars];
        int startX;
        int bottom = getHeight() - getPaddingBottom();
        int rtlFlag;
        final boolean isLayoutRtl = TextUtilsCompat.getLayoutDirectionFromLocale(Locale.getDefault()) == ViewCompat.LAYOUT_DIRECTION_RTL;
        if (isLayoutRtl) {
            rtlFlag = -1;
            startX = (int) (getWidth() - ViewCompat.getPaddingStart(this) - charSize);
        } else {
            rtlFlag = 1;
            startX = ViewCompat.getPaddingStart(this);
        }
        for (int i = 0; i < numChars; i++) {
            lineCoords[i] = new RectF(startX, bottom, startX + charSize, bottom);
            if (pinBackground != null) {
                if (isDigitSquare) {
                    lineCoords[i].top = getPaddingTop();
                    lineCoords[i].right = startX + lineCoords[i].height();
                } else {
                    lineCoords[i].top -= textHeight.height() + textBottomPadding * 2;
                }
            }

            if (space < 0) {
                startX += rtlFlag * charSize * 2;
            } else {
                startX += rtlFlag * (charSize + space);
            }
            charBottom[i] = lineCoords[i].bottom - textBottomPadding;
        }
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        onClickListener = l;
    }


    public void setOnPinEnteredListener(OnPinEnteredListener l) {
        onPinEnteredListener = l;
    }

    @Override
    public void setCustomSelectionActionModeCallback(ActionMode.Callback actionModeCallback) {
        throw new RuntimeException("setCustomSelectionActionModeCallback() not supported.");
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //super.onDraw(canvas);
        CharSequence text = getFullText();
        int textLength = text.length();
        float[] textWidths = new float[textLength];
        getPaint().getTextWidths(text, 0, textLength, textWidths);

        float hintWidth = 0;
        if (singleCharHint != null) {
            float[] hintWidths = new float[singleCharHint.length()];
            getPaint().getTextWidths(singleCharHint, hintWidths);
            for (float i : hintWidths) {
                hintWidth += i;
            }
        }
        for (int i = 0; i < numChars; i++) {
            //If a background for the pin characters is specified, it should be behind the characters.
            if (pinBackground != null) {
                updateDrawableState(i < textLength, i == textLength);
                pinBackground.setBounds((int) lineCoords[i].left, (int) lineCoords[i].top, (int) lineCoords[i].right, (int) lineCoords[i].bottom);
                pinBackground.draw(canvas);
            }
            float middle = lineCoords[i].left + charSize / 2;
            if (textLength > i) {
                if (!animate || i != textLength - 1) {
                    canvas.drawText(text, i, i + 1, middle - textWidths[i] / 2, charBottom[i], charPaint);
                } else {
                    canvas.drawText(text, i, i + 1, middle - textWidths[i] / 2, charBottom[i], lastCharPaint);
                }
            } else if (singleCharHint != null) {
                canvas.drawText(singleCharHint, middle - hintWidth / 2, charBottom[i], singleCharPaint);
            }
            //The lines should be in front of the text (because that's how I want it).
            if (pinBackground == null) {
                updateColorForLines(i <= textLength);
                canvas.drawLine(lineCoords[i].left, lineCoords[i].top, lineCoords[i].right, lineCoords[i].bottom, linesPaint);
            }
        }
    }

    private CharSequence getFullText() {
        if (mask == null) {
            return getText();
        } else {
            return getMaskChars();
        }
    }

    private StringBuilder getMaskChars() {
        if (maskChars == null) {
            maskChars = new StringBuilder();
        }
        int textLength = getText().length();
        while (maskChars.length() != textLength) {
            if (maskChars.length() < textLength) {
                maskChars.append(mask);
            } else {
                maskChars.deleteCharAt(maskChars.length() - 1);
            }
        }
        return maskChars;
    }


    private int getColorForState(int... states) {
        return colorStateList.getColorForState(states, Color.GRAY);
    }

    protected void updateColorForLines(boolean hasTextOrIsNext) {
        if (hasError) {
            linesPaint.setColor(getColorForState(android.R.attr.state_active));
        } else if (isFocused()) {
            linesPaint.setStrokeWidth(lineStrokeSelected);
            linesPaint.setColor(getColorForState(android.R.attr.state_focused));
            if (hasTextOrIsNext) {
                linesPaint.setColor(getColorForState(android.R.attr.state_selected));
            }
        } else {
            linesPaint.setStrokeWidth(lineStroke);
            linesPaint.setColor(getColorForState(-android.R.attr.state_focused));
        }
    }

    protected void updateDrawableState(boolean hasText, boolean isNext) {
        if (hasError) {
            pinBackground.setState(new int[]{android.R.attr.state_active});
        } else if (isFocused()) {
            pinBackground.setState(new int[]{android.R.attr.state_focused});
            if (isNext) {
                pinBackground.setState(new int[]{android.R.attr.state_focused, android.R.attr.state_selected});
            } else if (hasText) {
                pinBackground.setState(new int[]{android.R.attr.state_focused, android.R.attr.state_checked});
            }
        } else {
            pinBackground.setState(new int[]{-android.R.attr.state_focused});
        }
    }

    @Override
    protected void onTextChanged(CharSequence text, final int start, int lengthBefore, final int lengthAfter) {
        if (lineCoords == null || !animate) {
            if (onPinEnteredListener != null && text.length() == maxLength) {
                onPinEnteredListener.onPinEntered(text);
            }
            return;
        }

        if (animatedType == -1) {
            invalidate();
            return;
        }

        if (lengthAfter > lengthBefore) {
            if (animatedType == 0) {
                animatePopIn();
            } else {
                animateBottomUp(text, start);
            }
        }
    }

    private void animatePopIn() {
        ValueAnimator va = ValueAnimator.ofFloat(1, getPaint().getTextSize());
        va.setDuration(200);
        va.setInterpolator(new OvershootInterpolator());
        va.addUpdateListener(animation -> {
            lastCharPaint.setTextSize((Float) animation.getAnimatedValue());
            PinEditText.this.invalidate();
        });
        if (getText().length() == maxLength && onPinEnteredListener != null) {
            va.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    onPinEnteredListener.onPinEntered(getText());
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
        }
        va.start();
    }

    private void animateBottomUp(CharSequence text, final int start) {
        charBottom[start] = lineCoords[start].bottom - textBottomPadding;
        ValueAnimator animUp = ValueAnimator.ofFloat(charBottom[start] + getPaint().getTextSize(), charBottom[start]);
        animUp.setDuration(300);
        animUp.setInterpolator(new OvershootInterpolator());
        animUp.addUpdateListener(animation -> {
            Float value = (Float) animation.getAnimatedValue();
            charBottom[start] = value;
            PinEditText.this.invalidate();
        });

        lastCharPaint.setAlpha(255);
        ValueAnimator animAlpha = ValueAnimator.ofInt(0, 255);
        animAlpha.setDuration(300);
        animAlpha.addUpdateListener(animation -> {
            Integer value = (Integer) animation.getAnimatedValue();
            lastCharPaint.setAlpha(value);
        });

        AnimatorSet set = new AnimatorSet();
        if (text.length() == maxLength && onPinEnteredListener != null) {
            set.addListener(new Animator.AnimatorListener() {

                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    onPinEnteredListener.onPinEntered(getText());
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }
        set.playTogether(animUp, animAlpha);
        set.start();
    }

    public interface OnPinEnteredListener {
        void onPinEntered(CharSequence str);
    }
}