package com.social.app.ui.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.social.base.eventbus.RxBus
import com.social.base.preferences.PrefsManager
import com.social.base.utils.AndroidVersionUtil
import com.social.app.R
import com.social.app.api.models.alarms.NotificationsModel
import com.social.app.databinding.ActivityMainBinding
import com.social.app.events.ActivateServiceEvent
import com.social.app.events.NotificationEvent
import com.social.app.managers.ProfileManager
import com.social.app.managers.preferences.ConstantPreferences
import com.social.app.providers.NotificationProvider
import com.social.app.services.BluetoothLEService
import com.social.app.services.LocationServiceApi
import com.social.app.ui.BaseActivity
import com.social.app.ui.alarmInProgress.AlarmInProgressFragment
import com.social.app.ui.fragments.DashboardFragment
import com.social.app.ui.fragments.MenuFragment
import com.social.app.ui.sharePosition.SharePositionInProgressFragment
import com.social.app.ui.views.FooterView
import com.social.app.utils.ServiceUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class MainActivity : BaseActivity() {

    private val binding: ActivityMainBinding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }

    private val userActivate = BehaviorSubject.createDefault(false)

    private val dashboardFragment: DashboardFragment by lazy {
        DashboardFragment()
    }

    @Inject
    lateinit var rxBus: RxBus

    private val viewModel: MainViewModel by lazy {
        MainViewModel()
    }

    val observableUserState: Observable<Boolean>
        get() = userActivate.observeOn(AndroidSchedulers.mainThread())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component.inject(this)

        initFragments()

        addInitFragment()

        setupListeners()
    }

    @SuppressLint("CheckResult")
    private fun initFragments() {

        replaceFragment(dashboardFragment)

        val menuFragment = MenuFragment()
        replaceFragment(menuFragment)

        dashboardFragment.observableSwitchState.subscribe { this.attemptToChangeUserState(it) }
        menuFragment.observableSwitchState.subscribe { this.attemptToChangeUserState(it) }
    }

    private fun setupListeners() {
        binding.footerView.setOnClickListener { v ->
            val tag = v.tag as Int
            when (tag) {
                FooterView.INIT -> addInitFragment()
                FooterView.MENU -> addMenuFragment()
            }
        }
    }

    private fun addInitFragment() {
        binding.containerInit.visibility = View.VISIBLE
        binding.containerMenu.visibility = View.GONE
        validateState()
        clearBackStack()
    }

    private fun addMenuFragment() {
        binding.containerMenu.visibility = View.VISIBLE
        binding.containerInit.visibility = View.GONE
        validateState()
        clearBackStack()
    }

    private fun attemptToChangeUserState(notification: NotificationProvider<Boolean>) {

        if (!notification.hasError()) {
            val isActive = ProfileManager.isUserActive
            if (notification.`object` && isActive) {
                ServiceUtils.stopService(LocationServiceApi::class.java)
                ServiceUtils.stopService(BluetoothLEService::class.java)
                PrefsManager.getInstance().set(ConstantPreferences.TIMER_VISIT, 0L)
            } else {
                ServiceUtils.restartService(LocationServiceApi::class.java)
                ServiceUtils.restartService(BluetoothLEService::class.java)
            }
            ProfileManager.activateOrDeactivateUser(isActive.not())
            viewModel.notifyStatusService(isActive.not())
            validateState()
        }
    }

    private fun validateState() {
        val isActive = ProfileManager.isUserActive
        if (userActivate.value != isActive) {
            userActivate.onNext(isActive)
            if (AndroidVersionUtil.hasLollipop()) {
                val color = if (isActive) R.color.blue_dark else R.color.red
                window.statusBarColor = ContextCompat.getColor(this, color)
            }
        }
    }

    private fun validateLocationService() {
        if (ProfileManager.isUserActive && !ServiceUtils.isServiceRunning(LocationServiceApi::class.java)) {
            startService(Intent(this, LocationServiceApi::class.java))
        }
    }

    private fun validateBluetoothService() {
        if (ProfileManager.isUserActive && !ServiceUtils.isServiceRunning(BluetoothLEService::class.java)) {
            startService(Intent(this, BluetoothLEService::class.java))
        }
    }

    override fun onBackStackChanged(currentFragments: Int) {
        if (currentFragments == 0) {
            validateState()
        }
    }

    override fun onResume() {
        super.onResume()
        validateState()
        validateLocationService()
        validateBluetoothService()
        registerEvents()
    }

    private fun registerEvents() {
        subscriptions.add(rxBus.register(NotificationEvent::class.java) { event ->
            if (event.update) {
                alarmReceive()
            }
        })
        subscriptions.add(rxBus.register(ActivateServiceEvent::class.java) {
            validateState()
        })
    }

    private fun alarmReceive() {
        showInfoMessage(binding.root, getString(R.string.alarm_message_notification_receive))
    }

    fun showAlarmInProgressFragment(data: NotificationsModel) {
        addFragment(AlarmInProgressFragment.newInstance(data), R.id.container, true)
    }

    fun showSharePositionInProgressFragment(data: NotificationsModel) {
        addFragment(SharePositionInProgressFragment.newInstance(data), R.id.container, true)
    }

    fun showSharePositionFragment() {
        startActivity(Intent(applicationContext, SharePositionActivity::class.java))
    }

    companion object {
        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }
    }
}