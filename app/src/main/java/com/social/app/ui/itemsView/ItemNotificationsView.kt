package com.social.app.ui.itemsView

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.social.base.ui.adapters.GenericAdapter
import com.social.base.ui.interfaces.GenericItemView
import com.social.base.utils.DateUtils
import com.social.base.utils.StringUtil
import com.social.app.R
import com.social.app.api.models.alarms.NotificationsModel
import com.social.app.databinding.ViewMyNotificationBinding
import com.social.app.databinding.ViewNotificationBinding
import com.social.app.ui.adapters.SwipeItemAdapterListener
import com.social.app.ui.views.CustomSwipeLayout
import com.social.app.utils.COMPARTIR
import com.social.app.utils.PANIC
import com.social.app.utils.typeNotification

@SuppressLint("ViewConstructor")
class ItemNotificationsView(
        context: Context,
        isSwipe: Boolean = false,
        listener: SwipeNotificationListener? = null
) : FrameLayout(context), GenericItemView<NotificationsModel> {

    private val binding: ViewNotificationBinding by lazy {
        ViewNotificationBinding.inflate(LayoutInflater.from(context), this, true)
    }

    private val bindingMyNotification: ViewMyNotificationBinding by lazy {
        ViewMyNotificationBinding.inflate(LayoutInflater.from(context), this, true)
    }

    private var listener: SwipeNotificationListener? = null

    private val swipeLayout: CustomSwipeLayout by lazy {
        bindingMyNotification.swipeLayoutOrder
    }

    private var swipeItemAdapterListener: SwipeItemAdapterListener? = null

    var image = ObservableInt(0)

    var description = ObservableField<CharSequence>()

    var type = ObservableField("")

    val fileImage = ObservableField("")

    val fileImageVisibility = ObservableBoolean()

    private lateinit var notificationModel: NotificationsModel

    init {
        if (isSwipe) {
            bindingMyNotification.item = this
            handleSwipe()
        } else {
            binding.item = this
        }
        this.listener = listener
    }

    override fun bind(notificationModel: NotificationsModel) {
        this.notificationModel = notificationModel

        val comment = notificationModel.comment ?: ""

        description.set(
                TextUtils.concat(
                        StringUtil.applySpans(notificationModel.names, RelativeSizeSpan(1.2f), StyleSpan(Typeface.BOLD)), "\n",
                        StringUtil.applySpans(notificationModel.telephone, StyleSpan(Typeface.BOLD), ForegroundColorSpan(ContextCompat.getColor(context, R.color.gray))), "\n",
                        StringUtil.applySpans(DateUtils.getReadableDate(notificationModel.created), StyleSpan(Typeface.BOLD), ForegroundColorSpan(ContextCompat.getColor(context, R.color.gray))), "\n",
                        StringUtil.applySpans(comment, StyleSpan(Typeface.BOLD), ForegroundColorSpan(ContextCompat.getColor(context, R.color.gray))))
        )
        type.set(notificationModel.idNotification?.typeNotification(context))
        when (notificationModel.idNotification) {
            PANIC -> image.set(R.drawable.ic_sos_list)
            COMPARTIR -> {
                image.set(R.drawable.ic_share_position_list)
                fileImage.set(notificationModel.photo)
            }
            else -> {
                image.set(R.drawable.ic_share_position_list)
            }
        }

        fileImageVisibility.set(!notificationModel.photo.isNullOrEmpty())
    }

    private fun handleSwipe() {
        swipeLayout.isLeftSwipeEnabled = false
        swipeLayout.isRightSwipeEnabled = false
        swipeLayout.addDrag(CustomSwipeLayout.DragEdge.Left, findViewById(R.id.layout_container_notification))
        setupListeners()
    }

    private fun setupListeners() {

        swipeLayout.setOnClickListener {
            if (swipeLayout.openStatus === CustomSwipeLayout.Status.Close) {
                swipeLayout.open(CustomSwipeLayout.DragEdge.Left)
            }
        }

        if (swipeItemAdapterListener != null) {
            swipeLayout.removeSwipeListener(swipeItemAdapterListener)
        }

        swipeItemAdapterListener = object : SwipeItemAdapterListener() {
            override fun onOpen(layout: CustomSwipeLayout) {
                if (listener != null) {
                    listener?.onSwipeOrder(this@ItemNotificationsView)
                }
            }
        }

        swipeLayout.addSwipeListener(swipeItemAdapterListener)
    }

    fun onClicked() {
        listener?.onClickedDelete(this@ItemNotificationsView)
    }

    override fun getData(): NotificationsModel = notificationModel

    override fun setItemClickListener(onItemClickListener: GenericAdapter.OnItemClickListener?) {}

    override fun getIdForClick() = 0

    interface SwipeNotificationListener {

        fun onSwipeOrder(itemNotificationsView: ItemNotificationsView)

        fun onClickedDelete(itemNotificationsView: ItemNotificationsView)

    }

}