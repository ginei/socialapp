package com.social.app.ui.alarmInProgress

import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.*
import com.social.base.extensions.MEDIUM_DURATION
import com.social.base.extensions.SHORT_MEDIUM_DURATION
import com.social.base.extensions.hide
import com.social.base.extensions.show
import com.social.base.utils.DateUtils
import com.social.app.R
import com.social.app.api.models.Position
import com.social.app.api.models.alarms.NotificationsModel
import com.social.app.databinding.FragmentAlarmInProgressBinding
import com.social.app.managers.LocationManager
import com.social.app.ui.BaseFragment
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat

class AlarmInProgressFragment : BaseFragment() {

    val binding: FragmentAlarmInProgressBinding by lazy {
        FragmentAlarmInProgressBinding.inflate(LayoutInflater.from(context))
    }

    private val userModel: NotificationsModel by lazy {
        arguments?.getParcelable(NOTIFICATION) ?: NotificationsModel()
    }

    val viewModel: AlarmInProgressViewModel by lazy {
        AlarmInProgressViewModel(userModel)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    private lateinit var map: GoogleMap

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.viewModel = viewModel
        initMap()
        return binding.root
    }

    private fun subscribe() {
        subscriptions.addAll(viewModel.positionUpdate().subscribe(this::updatePosition),
                viewModel.getActions().subscribe(this::handleActions))
    }

    private fun handleActions(action: AlarmUiModel) {
        when (action) {
            AlarmUiModel.Zoom -> enablePinTweaks()
            AlarmUiModel.ZoomOut -> onLocationUpdateEnd()
        }
    }

    private fun initMap() {
        binding.mapView.onCreate(null)
        binding.mapView.getMapAsync({ googleMap ->
            map = googleMap
            updatePosition()
        })
        MapsInitializer.initialize(this.activity)
    }

    private var onlyFirstMoveCam = false

    private fun updatePosition(currentPosition: Position? = null) {

        map.clear()
        val markers: ArrayList<Marker> = ArrayList()

        val storekeeperLocation = LatLng(LocationManager.getLastLatitude(), LocationManager.getLastLongitude())
        markers.add(map.addMarker(MarkerOptions()
                .position(storekeeperLocation)
                .icon(bitmapDescriptorFromVector(R.drawable.ic_position_blue))
                .title("Mi posición")))

        markers.add(map.addMarker(MarkerOptions()
                .position(viewModel.location)
                .icon(bitmapDescriptorFromVector(R.drawable.ic_marker_alert))
                .title("Posición de la alarma")
                .snippet("Fecha: " + DateUtils.getReadableDate(viewModel.notificationModel.created))))

        currentPosition?.let {
            val currentDate = DateUtils.toCalendar(currentPosition.deviceTime, DateUtils.READABLE_FORMAT_DATE)

            markers.add(map.addMarker(MarkerOptions()
                    .position(LatLng(currentPosition.latitude, currentPosition.longitude))
                    .icon(bitmapDescriptorFromVector(R.drawable.ic_position_green))
                    .title("Posición actual")
                    .snippet("Fecha: " + currentDate))
            )
            viewModel.loadData(currentDate, currentPosition.latitude, currentPosition.longitude)
        }

        if (!onlyFirstMoveCam) {
            onlyFirstMoveCam = true
            moveCam(markers)
        }
    }

    private fun moveCam(markers: ArrayList<Marker>) {
        val builder = LatLngBounds.Builder()
        for (marker in markers) {
            builder.include(marker.position)
        }
        val bounds = builder.build()

        val padding = 100
        val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
        map.animateCamera(cu)
    }

    private fun enablePinTweaks() {
        binding.imageViewClose.show()
        val clpcontactUs = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, 0)
        binding.layoutMapContainer.apply {
            layoutParams = clpcontactUs
        }
        binding.imageViewExpand.hide()
        binding.buttonGoogleMaps.hide()
        binding.constraintData.hide()
        binding.guideline.hide()
        binding.viewLine.hide()
        val valueAnimatorTop = ValueAnimator.ofInt(0, 0/*binding.layoutParent.measuredHeight - binding.mapView.measuredHeight - binding.viewContainerOptions.measuredHeight*/)
        valueAnimatorTop.addUpdateListener { animation -> animateTopChange(-(animation.animatedValue as Int)) }

        val valueAnimatorBottom = ValueAnimator.ofInt(0, binding.viewContainerOptions.measuredHeight)
        valueAnimatorBottom.addUpdateListener { animation -> animateBottomChange(-(animation.animatedValue as Int)) }

        val animatorSet = AnimatorSet()
        animatorSet.duration = MEDIUM_DURATION.toLong()
        animatorSet.playTogether(valueAnimatorTop, valueAnimatorBottom)

        animatorSet.start()
    }

    private fun onLocationUpdateEnd() {
        binding.imageViewClose.hide()
        binding.imageViewExpand.show()
        binding.buttonGoogleMaps.show()
        binding.constraintData.show()
        binding.viewLine.show()
        binding.guideline.show()

        val valueAnimatorTop = ValueAnimator.ofInt((binding.mapView.layoutParams as RelativeLayout.LayoutParams).topMargin, 0)
        valueAnimatorTop.addUpdateListener { animation -> animateTopChange((animation.animatedValue as Int)) }

        val valueAnimatorBottom = ValueAnimator.ofInt((binding.mapView.layoutParams as RelativeLayout.LayoutParams).bottomMargin, 0)
        valueAnimatorBottom.addUpdateListener { animation -> animateBottomChange((animation.animatedValue as Int)) }

        val animatorSet = AnimatorSet()
        animatorSet.duration = SHORT_MEDIUM_DURATION.toLong()
        animatorSet.playTogether(valueAnimatorTop, valueAnimatorBottom)
        animatorSet.start()
    }

    private fun animateTopChange(value: Int) {
        val layoutParams = binding.mapView.layoutParams as RelativeLayout.LayoutParams
        layoutParams.setMargins(0, value, 0, layoutParams.bottomMargin)
        binding.mapView.layoutParams = layoutParams
    }

    private fun animateBottomChange(value: Int) {
        val layoutParams = binding.mapView.layoutParams as RelativeLayout.LayoutParams
        layoutParams.setMargins(0, layoutParams.topMargin, 0, value)
        binding.mapView.layoutParams = layoutParams
    }

    companion object {
        private val NOTIFICATION = "notification"

        @JvmStatic
        fun newInstance(notificationsModel: NotificationsModel): AlarmInProgressFragment {
            val alarmInProgressFragment = AlarmInProgressFragment()
            val arguments = Bundle()
            arguments.putParcelable(NOTIFICATION, notificationsModel)
            alarmInProgressFragment.arguments = arguments
            return alarmInProgressFragment
        }
    }

    override fun onResume() {
        super.onResume()
        binding.mapView.onResume()
        subscribe()
        viewModel.subscribe()
    }

    override fun onPause() {
        binding.mapView.onPause()
        super.onPause()
    }

    override fun onDestroyView() {
        binding.mapView.onDestroy()
        super.onDestroyView()
    }

    private fun bitmapDescriptorFromVector(vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = VectorDrawableCompat.create(resources, vectorResId, null)

        var bitmap: Bitmap? = null
        vectorDrawable?.apply {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            draw(canvas)
        }
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }
}