package com.social.app.ui.viewModels;

import androidx.databinding.ObservableField;

import com.social.app.api.models.ProfileResponse;
import com.social.app.managers.ProfileManager;

public class ProfileViewModel extends BaseViewModel {

    public ObservableField<String> id = new ObservableField<>();

    public ObservableField<String> telephone = new ObservableField<>();

    public ObservableField<String> name = new ObservableField<>();

    public ObservableField<String> imgProfile = new ObservableField<>();

    public ProfileViewModel() {
        getProfile();
    }

    private void getProfile() {
        ProfileResponse profile = ProfileManager.INSTANCE.getUserProfile();
        name.set(profile.getName());
        telephone.set(profile.getPhone());

    }


}
