package com.social.app.ui.views;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.view.LayoutInflater;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.social.app.R;
import com.social.app.databinding.ViewCustomMarkerBinding;


public class CustomMarkerView extends MarkerView {

    private ViewCustomMarkerBinding binding;

    public CustomMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.view_custom_marker, this, true);
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        String value = String.valueOf(e.getY());

        if (value.equals("0.0")) {
            value = "0";
        }
        binding.textViewContent.setText(value);
        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}