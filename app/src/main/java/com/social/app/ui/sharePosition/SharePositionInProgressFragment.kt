package com.social.app.ui.sharePosition

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.social.app.api.models.alarms.NotificationsModel
import com.social.app.databinding.FragmentSharePositionInProgressBinding
import com.social.app.ui.BaseFragment

class SharePositionInProgressFragment : BaseFragment() {

    companion object {
        private const val NOTIFICATION = "notification"

        @JvmStatic
        fun newInstance(notificationsModel: NotificationsModel): SharePositionInProgressFragment {
            val sharePositionInProgressFragment = SharePositionInProgressFragment()
            val arguments = Bundle()
            arguments.putParcelable(NOTIFICATION, notificationsModel)
            sharePositionInProgressFragment.arguments = arguments
            return sharePositionInProgressFragment
        }
    }

    val binding: FragmentSharePositionInProgressBinding by lazy {
        FragmentSharePositionInProgressBinding.inflate(LayoutInflater.from(context))
    }

    private val userModel: NotificationsModel by lazy {
        arguments?.getParcelable<NotificationsModel>(NOTIFICATION)!!
    }

    val viewModel: SharePositionInProgressViewModel by lazy {
        SharePositionInProgressViewModel(userModel)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.viewModel = viewModel
        return binding.root
    }

}