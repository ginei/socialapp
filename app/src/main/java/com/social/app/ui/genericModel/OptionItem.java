package com.social.app.ui.genericModel;

public class OptionItem {

    private CharSequence text;
    private int drawable;
    private int tag;

    public OptionItem(CharSequence text, int tag) {
        this.text = text;
        this.tag = tag;
    }

    public OptionItem(CharSequence text, int drawable, int tag) {
        this.text = text;
        this.drawable = drawable;
        this.tag = tag;
    }

    public int getDrawable() {
        return drawable;
    }

    public CharSequence getText() {
        return text;
    }

    public int getTag() {
        return tag;
    }
}