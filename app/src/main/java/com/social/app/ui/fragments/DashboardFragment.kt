package com.social.app.ui.fragments

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.social.base.eventbus.RxBus
import com.social.base.extensions.beatAnimation
import com.social.base.ui.adapters.GenericAdapter
import com.social.base.ui.factories.GenericAdapterFactory
import com.social.base.ui.interfaces.GenericItemAbstract
import com.social.base.ui.interfaces.GenericItemView
import com.social.app.R
import com.social.app.api.models.alarms.NotificationsModel
import com.social.app.databinding.FragmentDashboardBinding
import com.social.app.events.NotificationEvent
import com.social.app.ui.activities.MainActivity
import com.social.app.ui.itemsView.ItemNotificationsView
import com.social.app.ui.viewModels.AlarmViewModel
import com.social.app.utils.PANIC
import javax.inject.Inject

class DashboardFragment : HomeBaseFragment() {

    private val adapter: GenericAdapter  by lazy {
        GenericAdapter(object : GenericAdapterFactory() {

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericItemView<*> {
                val itemNotificationsView = ItemNotificationsView(parent.context)
                itemNotificationsView.setOnClickListener {
                    if (itemNotificationsView.data.idNotification == PANIC) {
                        (activity as MainActivity).showAlarmInProgressFragment(itemNotificationsView.data)
                    } else {
                        (activity as MainActivity).showSharePositionInProgressFragment(itemNotificationsView.data)
                    }
                }
                return itemNotificationsView
            }
        })
    }

    @Inject
    lateinit var rxBus: RxBus

    init {
        adapter.setHasStableIds(true)
    }

    val binding: FragmentDashboardBinding by lazy {
        FragmentDashboardBinding.inflate(LayoutInflater.from(context))
    }

    val viewModel: AlarmViewModel by lazy {
        AlarmViewModel()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
        binding.viewModel = viewModel
        setupRecyclerView()
        viewModel.getNotifications()
    }

    private fun setupRecyclerView() {
        binding.recyclerViewNotifications.apply {
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            adapter = this@DashboardFragment.adapter
        }
    }

    private fun subscribe() {

        subscriptions.add(viewModel.alarmObservable().subscribe { showNotification() })

        subscriptions.add(viewModel.shareLocationObservable().subscribe { (activity as MainActivity).showSharePositionFragment() })

        subscriptions.add(viewModel.observableSnackBar().subscribe { snackBarEvent -> showError(binding.root, snackBarEvent.message) })

        subscriptions.add(viewModel.observableShowLoading().subscribe { showLoading(it) })

        subscriptions.add(viewModel.observableNotificationsItems().subscribe { loadItemsAlarm(it) })

        subscriptions.add(rxBus.register(NotificationEvent::class.java) { if (it.update) alarmReceive() })

        initBaseActions()

        viewModel.subscribe()
    }

    private fun initBaseActions() {
        subscriptions.addAll(
                viewModel.getActions().subscribe(this::handleViewModelActions)
        )
    }

    private fun handleViewModelActions(action: AlarmViewModel.DashBoardUiModel) {
        when (action) {
            is AlarmViewModel.DashBoardUiModel.AnimationFloatButton -> animateActionFabIcon(action.visibility)
        }
    }

    private fun animateActionFabIcon(visibility: Boolean) {
        val icon = if (visibility) "ic_fab_icon" else "ic_close"

        binding.fabActions.beatAnimation(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator) {
                binding.fabActions.setImageResource(resources.getIdentifier(icon, "drawable", context?.packageName))
            }

            override fun onAnimationEnd(animation: Animator?) {
                binding.fabActions.setImageResource(resources.getIdentifier(icon, "drawable", context?.packageName))
            }
        })
    }

    private fun alarmReceive() {
        viewModel.getNotifications()
    }

    private fun showNotification() {
        showInfoMessage(binding.root, getString(R.string.alarm_message_notification_send))
    }

    override fun getView(): View? = binding.root

    private fun loadItemsAlarm(items: List<NotificationsModel>) {
        items.filter { it.visibilitySystem }.apply {
            adapter.clearItems()
            viewModel.quantityNotification.set(getString(R.string.alarm_quantity, this.size))
            this.forEach {
                adapter.addItem(GenericItemAbstract(it))
            }
            viewModel.isNotificationsVisible.set(viewModel.alertProvider.getAlertList()?.isNotEmpty()
                    ?: false)
        }
    }

    override fun onResume() {
        super.onResume()
        subscribe()
    }

    override fun getContainer() = R.id.container_init
}

