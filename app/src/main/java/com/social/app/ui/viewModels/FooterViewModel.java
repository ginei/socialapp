package com.social.app.ui.viewModels;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

public class FooterViewModel extends BaseViewModel {

    private BehaviorSubject<Integer> ordersAvailable = BehaviorSubject.createDefault(0);
    private BehaviorSubject<Integer> ordersInProgress = BehaviorSubject.createDefault(0);
    private PublishSubject<Integer> chatMessages = PublishSubject.create();

    public FooterViewModel() {

        getComponent().inject(this);

    }


    public Observable<Integer> getOrdersAvailable() {
        return ordersAvailable.observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Integer> getOrdersInProgress() {
        return ordersInProgress.observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Integer> getChatMessages() {
        return chatMessages.observeOn(AndroidSchedulers.mainThread());
    }
}
