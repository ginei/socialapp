package com.social.app.ui.viewModels

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import android.text.TextUtils
import com.social.base.utils.FileUtil
import com.social.base.utils.ImageUtil
import com.social.app.R
import com.social.app.api.controllers.GeocodingController
import com.social.app.api.controllers.RastreoApiController
import com.social.app.managers.FirebaseStorageManager
import com.social.app.managers.LocationManager
import com.social.app.utils.COMPARTIR
import com.social.app.utils.Constants
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.io.File
import javax.inject.Inject

class SharePositionViewModel : BaseViewModel() {

    @Inject
    lateinit var rastreoApiController: RastreoApiController

    @Inject
    lateinit var geocodingController: GeocodingController

    val fileImage = ObservableField<File>()
    val onlySystem = ObservableBoolean(false)

    private var urlPhotoFirebase: String? = null

    private var reducedFile: File? = null

    private val openCameraSubject = PublishSubject.create<Boolean>()

    var description = ObservableField("")

    private var successAlarm = PublishSubject.create<Unit>()

    init {
        component.inject(this)
    }

    fun takePhoto() {
        openCameraSubject.onNext(true)
    }

    fun send() {
        showLoading()
        if (urlPhotoFirebase.isNullOrEmpty() && reducedFile != null) {
            FirebaseStorageManager.attemptToUploadPhoto(FileUtil.getPathForUserFolder(), reducedFile!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ successLoadPhotoToFirebase(it) }, { showServiceError(it) })
        } else {
            successLoadPhotoToFirebase()
        }
    }

    private fun successLoadPhotoToFirebase(urlPhotoFirebase: String = "") {
        hideLoading()
        this.urlPhotoFirebase = urlPhotoFirebase

        if (!TextUtils.isEmpty(description.get())) {
            val latitude = LocationManager.getLastLatitude()
            val longitude = LocationManager.getLastLongitude()
            getAddress(description.get()?: "", urlPhotoFirebase, COMPARTIR, latitude, longitude)
        } else {
            showSnackBarError(R.string.error_empty_fields)
        }
    }

    private fun getAddress(description: String = "", urlPhotoFirebase: String, notificationId: Int, latitude: Double, longitude: Double) {
        showLoading()
        disposables.add(geocodingController.getAddress(latitude, longitude)
                .subscribe({ hideLoading(); sendNotification(description, urlPhotoFirebase, it, notificationId, latitude, longitude) }, { showServiceError(it) }))
    }

    private fun sendNotification(description: String = "", urlPhotoFirebase: String, address: String, notificationId: Int, latitude: Double, longitude: Double) {
        showLoading()
        disposables.add(rastreoApiController.sharePosition(description, urlPhotoFirebase, address, notificationId, latitude, longitude, onlySystem.get())
                .subscribe({ hideLoading(); successAlarm.onNext(Unit) }, { this.showServiceError(it) }))
    }

    fun alarmObservable(): Observable<Unit> =
            successAlarm.hide().observeOn(AndroidSchedulers.mainThread())

    fun getOpenCameraSubject(): Observable<Boolean> =
            openCameraSubject.observeOn(AndroidSchedulers.mainThread())

    fun loadImageFile() {
        reducedFile = ImageUtil.resizeImage(
                FileUtil.getLastFileModifiedOnFolder(FileUtil.getPathForUserFolder()),
                Constants.MAX_PHOTO_RESOLUTION, Constants.MAX_PHOTO_RESOLUTION)
        fileImage.set(reducedFile)
    }
}