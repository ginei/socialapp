package com.social.app.ui.itemsView;

import android.content.Context;
import android.content.res.Resources;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.social.base.ui.adapters.GenericAdapter;
import com.social.base.ui.interfaces.GenericItemView;
import com.social.app.ui.genericModel.DividerItem;


public class DividerView extends RelativeLayout implements GenericItemView<DividerItem> {

    private DividerItem dividerItem;

    public DividerView(Context context) {
        super(context);
        init();
    }

    public void init() {
        MarginLayoutParams marginLayoutParams = new MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        setLayoutParams(marginLayoutParams);
        setGravity(Gravity.CENTER_HORIZONTAL);
    }

    @Override
    public void bind(DividerItem dividerItem) {
        this.dividerItem = dividerItem;
        Resources resources = getResources();

        int marginTop = resources.getDimensionPixelSize(dividerItem.getMarginTop());
        int marginBottom = resources.getDimensionPixelSize(dividerItem.getMarginBottom());
        int marginLeft = resources.getDimensionPixelSize(dividerItem.getMarginLeft());
        int marginRight = resources.getDimensionPixelSize(dividerItem.getMarginRight());
        int thickness = resources.getDimensionPixelSize(dividerItem.getThickness());

        MarginLayoutParams marginLayoutParams = (MarginLayoutParams) getLayoutParams();
        marginLayoutParams.topMargin = marginTop;
        marginLayoutParams.bottomMargin = marginBottom;
        marginLayoutParams.leftMargin = marginLeft;
        marginLayoutParams.rightMargin = marginRight;

        setLayoutParams(marginLayoutParams);
        setPadding(thickness, thickness, thickness, thickness);
        setBackgroundResource(dividerItem.getColor());
    }

    @Override
    public DividerItem getData() {
        return dividerItem;
    }

    @Override
    public void setItemClickListener(GenericAdapter.OnItemClickListener onItemClickListener) {

    }

    @Override
    public int getIdForClick() {
        return 0;
    }
}