package com.social.app.ui.genericModel;

;import com.social.app.R;

public class DividerItem {

    private int thickness = R.dimen.spacing_xmicro;
    private int color = R.color.gray_light;
    private int marginTop = R.dimen.spacing_xlarge;
    private int marginBottom = R.dimen.spacing_xlarge;
    private int marginLeft = R.dimen.spacing_xlarge;
    private int marginRight = R.dimen.spacing_xlarge;

    public DividerItem() {
    }

    public int getThickness() {
        return thickness;
    }

    public DividerItem setThickness(int thickness) {
        this.thickness = thickness;
        return this;
    }

    public int getColor() {
        return color;
    }

    public DividerItem setColor(int color) {
        this.color = color;
        return this;
    }

    public int getMarginTop() {
        return marginTop;
    }

    public DividerItem setMarginTop(int marginTop) {
        this.marginTop = marginTop;
        return this;
    }

    public int getMarginBottom() {
        return marginBottom;
    }

    public DividerItem setMarginBottom(int marginBottom) {
        this.marginBottom = marginBottom;
        return this;
    }

    public int getMarginLeft() {
        return marginLeft;
    }

    public DividerItem setMarginLeft(int marginLeft) {
        this.marginLeft = marginLeft;
        return this;
    }

    public int getMarginRight() {
        return marginRight;
    }

    public DividerItem setMarginRight(int marginRight) {
        this.marginRight = marginRight;
        return this;
    }
}