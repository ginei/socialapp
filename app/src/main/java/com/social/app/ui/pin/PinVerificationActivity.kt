package com.social.app.ui.pin

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import com.social.base.preferences.PrefsManager
import com.social.app.R
import com.social.app.databinding.ActivityPinVerificationBinding
import com.social.app.managers.preferences.ConstantPreferences
import com.social.app.ui.BaseActivity
import com.social.app.ui.views.PinEditText
import javax.inject.Inject

private const val TYPE_PIN = "TYPE_PIN"
private const val PIN = "PIN"

class PinVerificationActivity : BaseActivity() {

    companion object {
        @JvmField
        val CREATE_PIN = "CREATE_PIN"

        @JvmField
        val VERIFY_PIN = "VERIFY_PIN"

        @JvmStatic
        fun newInstance(context: Context, type: String? = CREATE_PIN, pin: String? = ""): Intent {
            return Intent(context, PinVerificationActivity::class.java).apply {
                putExtra(TYPE_PIN, type)
                putExtra(PIN, pin)
            }
        }
    }

    @Inject
    lateinit var prefsManager: PrefsManager

    private val typePin: String by lazy { intent?.getStringExtra(TYPE_PIN) ?: "" }

    private val pinSaved: String by lazy { intent?.getStringExtra(PIN) ?: "" }

    private val pinEntry: PinEditText by lazy {
        binding.editTextPin
    }

    private var pinToCreate = ""

    private val binding: ActivityPinVerificationBinding by lazy {
        DataBindingUtil.setContentView<ActivityPinVerificationBinding>(this, R.layout.activity_pin_verification)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
        setupActionBar(binding.toolbar)
        init()
    }

    fun init() {
        if (typePin == CREATE_PIN) {
            binding.textViewTitle.text = getString(R.string.pin_create)
        } else if (typePin == VERIFY_PIN) {
            binding.textViewTitle.text = getString(R.string.pin_verify)
        }
        initListeners()
    }

    private fun initListeners() {
        pinEntry.setOnPinEnteredListener({ str -> validatePin(str.toString()) })
    }

    private fun validatePin(pin: String) {
        when (typePin) {
            CREATE_PIN -> {
                if (pinToCreate.isEmpty()) {
                    pinToCreate = pin
                    binding.textViewTitle.text = getString(R.string.pin_again)
                    pinEntry.postDelayed({ pinEntry.text = null }, 100)
                } else {
                    if (pin == pinToCreate) {
                        setPin(pin)
                        showToast(applicationContext, getString(R.string.pin_created))
                        setResult(Activity.RESULT_OK)
                        finish()
                    } else {
                        binding.textViewTitle.text = getString(R.string.pin_create)
                        pinToCreate = ""
                        showToast(applicationContext, getString(R.string.pin_incorrect_to_create))
                        pinEntry.postDelayed({ pinEntry.text = null }, 100)
                    }
                }
            }
            VERIFY_PIN -> {
                if (pin == pinSaved) {
                    showLoading(true)
                    setResult(Activity.RESULT_OK)
                    finish()
                } else {
                    showToast(applicationContext, getString(R.string.pin_incorrect))
                    pinEntry.postDelayed({ pinEntry.text = null }, 1000)
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            setResult(Activity.RESULT_CANCELED)
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setPin(pin: String) {
        prefsManager.set(ConstantPreferences.PIN, pin)
    }

}