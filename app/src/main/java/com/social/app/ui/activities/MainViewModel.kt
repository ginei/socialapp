package com.social.app.ui.activities

import com.social.app.api.controllers.GeocodingController
import com.social.app.api.controllers.RastreoApiController
import com.social.app.managers.LocationManager
import com.social.app.ui.viewModels.BaseViewModel
import com.social.app.utils.SERVICE_OFF
import com.social.app.utils.SERVICE_ON
import javax.inject.Inject

class MainViewModel : BaseViewModel() {

    @Inject
    lateinit var rastreoApiController: RastreoApiController

    @Inject
    lateinit var geocodingController: GeocodingController

    init {
        component.inject(this)
    }

    fun notifyStatusService(statusService: Boolean) {
        showLoading()
        val latitude = LocationManager.getLastLatitude()
        val longitude = LocationManager.getLastLongitude()
        val event = SERVICE_ON.takeIf { statusService }
                ?: SERVICE_OFF
        disposables.add(geocodingController.getAddress(latitude, longitude)
                .subscribe({ hideLoading(); sendNotification(event, it, latitude, longitude) }, { it.printStackTrace() }))
    }

    private fun sendNotification(notificationId: Int, address: String, latitude: Double, longitude: Double) {
        showLoading()
        disposables.add(rastreoApiController.createNotification(notificationId, address, latitude, longitude)
                .subscribe({ hideLoading(); }, { it.printStackTrace() }))
    }

}