package com.social.app.ui.sharePosition

import android.content.Intent
import androidx.databinding.ObservableField
import android.graphics.Typeface
import android.net.Uri
import androidx.core.content.ContextCompat
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import com.google.android.gms.maps.model.LatLng
import com.social.base.utils.DateUtils
import com.social.base.utils.StringUtil
import com.social.app.R
import com.social.app.api.models.alarms.NotificationsModel
import com.social.app.managers.LocationManager
import com.social.app.providers.ResourceProvider
import com.social.app.ui.GenericApplication
import com.social.app.utils.AppUtils
import com.social.app.utils.DistanceUtils
import com.social.base.extensions.biLet
import com.social.app.ui.viewModels.BaseViewModel
import java.util.*
import javax.inject.Inject

class SharePositionInProgressViewModel(private val notificationModel: NotificationsModel) : BaseViewModel() {

    var description: ObservableField<CharSequence> = ObservableField("")
    var distance: ObservableField<CharSequence> = ObservableField("")
    var address: ObservableField<CharSequence> = ObservableField("")
    var comment: ObservableField<CharSequence> = ObservableField("")

    var fileImage = ""

    @Inject
    lateinit var resource: ResourceProvider

    val location: LatLng by lazy {
        (notificationModel.latitude to notificationModel.longitude).biLet { latitude, longitude ->
            LatLng(latitude.toDouble(), longitude.toDouble())
        } ?: LatLng(0.0, 0.0)
    }

    init {
        component.inject(this)
        loadData()
    }

    private fun loadData(currentDate: String? = null, currentLat: Double? = null, currentLng: Double? = null) {
        val context = GenericApplication.get()

        val date = currentDate?.let { it } ?: run { DateUtils.getReadableDate(notificationModel.created) }
        description.set(TextUtils.concat(
                StringUtil.applySpans(notificationModel.names, RelativeSizeSpan(1.2f), StyleSpan(Typeface.BOLD)), "\n",
                StringUtil.applySpans(notificationModel.telephone, StyleSpan(Typeface.BOLD), ForegroundColorSpan(ContextCompat.getColor(context, R.color.gray))), "\n",
                StringUtil.applySpans(date, StyleSpan(Typeface.BOLD), ForegroundColorSpan(ContextCompat.getColor(context, R.color.gray))), "\n"))

        val lastLatitude = currentLat?.let { it } ?: run { location.latitude }
        val lastLongitude = currentLng?.let { it } ?: run { location.longitude }

        distance.set(resource.getString(R.string.alarm_distance_value,
                DistanceUtils.distanceBetweenInKms(LocationManager.getLastLatitude(), LocationManager.getLastLongitude(), lastLatitude, lastLongitude)))

        address.set(notificationModel.address)
        notificationModel.comment?.let { comment.set(it) }
        notificationModel.photo?.let { fileImage = it }
    }

    fun callUser() {
        AppUtils.callToNumber(notificationModel.telephone)
    }

    fun howToGet() {
        val uri = String.format(Locale.getDefault(), "https://www.google.com/maps/dir/?api=1&destination=%s,%s",
                location.latitude.toString(),
                location.longitude.toString())
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        GenericApplication.get().startActivity(intent)
    }

}
