package com.social.app.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.social.base.ui.adapters.GenericAdapter;
import com.social.base.ui.factories.GenericAdapterFactory;
import com.social.base.ui.interfaces.GenericItem;
import com.social.base.ui.interfaces.GenericItemAbstract;
import com.social.base.ui.interfaces.GenericItemView;
import com.social.app.BuildConfig;
import com.social.app.R;
import com.social.app.databinding.FragmentMenuBinding;
import com.social.app.managers.ProfileManager;
import com.social.app.ui.activities.ConfigurationActivity;
import com.social.app.ui.genericModel.DividerItem;
import com.social.app.ui.genericModel.OptionItem;
import com.social.app.ui.itemsView.DividerView;
import com.social.app.ui.login.LoginActivity;
import com.social.app.ui.myNotifications.MyNotificationsActivity;
import com.social.app.ui.pin.PinVerificationActivity;
import com.social.app.ui.viewModels.ProfileViewModel;
import com.social.app.ui.views.OptionProfileView;

import java.util.ArrayList;
import java.util.List;

public class MenuFragment extends HomeBaseFragment implements View.OnClickListener {

    private final static int TAG_DISTANCE = 1;
    private final static int TAG_CONFIGURATION = 2;
    private final static int TAG_MY_NOTIFICATIONS = 3;
    private final static int TAG_TERMS_AND_CONDITIONS = 4;
    private final static int TAG_PIN = 5;
    private final static int TAG_SIGN_OUT = 10;

    private FragmentMenuBinding binding;

    private GenericAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getComponent().inject(this);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public View getView() {
        binding = FragmentMenuBinding.inflate(LayoutInflater.from(getContext()));

        binding.setProfileViewModel(new ProfileViewModel());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRecyclerView();
        loadAppData();
    }

    private void setupRecyclerView() {
        RecyclerView recyclerViewOrders = binding.recyclerViewMenu;
        adapter = new GenericAdapter(new GenericAdapterFactory() {
            @Override
            public GenericItemView onCreateViewHolder(ViewGroup parent, int viewType) {
                switch (viewType) {
                    case TYPE_DIVIDER:
                        return new DividerView(parent.getContext());
                    default:
                        OptionProfileView optionProfileView = new OptionProfileView(parent.getContext());
                        optionProfileView.setOnClickListener(MenuFragment.this);
                        return optionProfileView;
                }
            }
        });

        recyclerViewOrders.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewOrders.setAdapter(adapter);

        loadMenu();
    }

    private void loadAppData() {
        binding.viewProfile.textViewAppData.setText(BuildConfig.FLAVOR + " " + BuildConfig.VERSION_NAME + " (" + BuildConfig.VERSION_CODE + ")");
    }

    private void loadMenu() {
        List<GenericItem> items = new ArrayList<>();

        items.add(new GenericItemAbstract(new DividerItem()
                .setMarginLeft(R.dimen.spacing_empty)
                .setMarginRight(R.dimen.spacing_empty)
                .setMarginTop(R.dimen.spacing_empty)
                .setMarginBottom(R.dimen.spacing_empty)
                .setThickness(R.dimen.spacing_large)
                .setColor(R.color.background), GenericAdapterFactory.TYPE_DIVIDER));

        items.add(new GenericItemAbstract(new OptionItem(getString(R.string.menu_distance), R.drawable.ic_distance, TAG_DISTANCE)));
        items.add(getDividerItem());

        items.add(new GenericItemAbstract(new OptionItem(getString(R.string.menu_my_notifications), R.drawable.ic_history, TAG_MY_NOTIFICATIONS)));
        items.add(getDividerItem());

        items.add(new GenericItemAbstract(new OptionItem(getString(R.string.menu_itag), R.drawable.ic_tag, TAG_CONFIGURATION)));
        items.add(getDividerItem());

        items.add(new GenericItemAbstract(new OptionItem(getString(R.string.menu_terms_and_conditions), R.drawable.ic_terms_and_conditions, TAG_TERMS_AND_CONDITIONS)));
        items.add(getDividerItem());

        items.add(new GenericItemAbstract(new OptionItem(getString(R.string.menu_pin), R.drawable.ic_terms_and_conditions, TAG_PIN)));
        items.add(getDividerItem());

        items.add(new GenericItemAbstract(new OptionItem(getString(R.string.menu_log_out), R.drawable.ic_log_out, TAG_SIGN_OUT)));

        items.add(new GenericItemAbstract(new DividerItem()
                .setMarginLeft(R.dimen.spacing_empty)
                .setMarginRight(R.dimen.spacing_empty)
                .setMarginTop(R.dimen.spacing_empty)
                .setMarginBottom(R.dimen.spacing_empty)
                .setThickness(R.dimen.spacing_large)
                .setColor(R.color.background), GenericAdapterFactory.TYPE_DIVIDER));

        adapter.setItems(items);
    }

    private GenericItemAbstract getDividerItem() {
        return new GenericItemAbstract(new DividerItem()
                .setMarginLeft(R.dimen.spacing_standard)
                .setMarginRight(R.dimen.spacing_standard)
                .setMarginTop(R.dimen.spacing_empty)
                .setMarginBottom(R.dimen.spacing_empty), GenericAdapterFactory.TYPE_DIVIDER);
    }

    private void logout() {
        ProfileManager.INSTANCE.logoutDevice();
        startActivity(new Intent(getContext(), LoginActivity.class));
        getActivity().finish();
    }

    @Override
    public void onClick(View view) {
        int tag = (int) view.getTag();
        switch (tag) {
            case TAG_CONFIGURATION:
                startActivity(new Intent(getActivity(), ConfigurationActivity.class));
                break;
            case TAG_MY_NOTIFICATIONS:
                startActivity(new Intent(getActivity(), MyNotificationsActivity.class));
                break;
            case TAG_PIN:
                startActivity(PinVerificationActivity.newInstance(getActivity(), PinVerificationActivity.CREATE_PIN, ""));
                break;
            case TAG_SIGN_OUT:
                logout();
                break;
        }
    }

    @Override
    public int getContainer() {
        return R.id.container_menu;
    }
}
