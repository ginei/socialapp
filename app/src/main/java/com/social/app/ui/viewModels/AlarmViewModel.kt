package com.social.app.ui.viewModels

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.social.app.R
import com.social.app.api.controllers.GeocodingController
import com.social.app.api.controllers.RastreoApiController
import com.social.app.api.models.alarms.NotificationsModel
import com.social.app.managers.LocationManager
import com.social.app.managers.ProfileManager
import com.social.app.providers.AlertProvider
import com.social.app.utils.COMPARTIR
import com.social.app.utils.PANIC
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.util.*
import javax.inject.Inject

class AlarmViewModel : BaseViewModel() {

    var myNotifications = false

    var quantityNotification = ObservableField("")

    var refreshing = ObservableBoolean()

    var subMenu = ObservableBoolean()

    @Inject
    lateinit var rastreoApiController: RastreoApiController

    @Inject
    lateinit var geocodingController: GeocodingController

    @Inject
    lateinit var alertProvider: AlertProvider

    private var successAlarm = PublishSubject.create<Unit>()

    private var shareLocation = PublishSubject.create<Unit>()

    var isNotificationsVisible = ObservableBoolean(false)

    private val notificationsItem = BehaviorSubject.createDefault(Collections.emptyList<NotificationsModel>())

    private val viewModelActions = PublishSubject.create<DashBoardUiModel>()

    init {
        component.inject(this)
    }

    fun subscribe() {
        disposables.add(alertProvider.onAlertListChange().subscribe({ updateNotificationView() }))
    }

    private fun updateNotificationView() {
        val list: List<NotificationsModel> = alertProvider.getGroupAlertList()
        notificationsItem.onNext(list)
    }

    fun showSubMenu() {
        val visibility = subMenu.get()
        viewModelActions.onNext(DashBoardUiModel.AnimationFloatButton(visibility))
        subMenu.set(visibility.not())
    }

    fun createPanic() {
        createNotification(PANIC)
    }

    fun createSharePosition() {
        createNotification(COMPARTIR)
    }

    fun createNotification(notificationId: Int) {
        showSubMenu()
        when {
            ProfileManager.isUserActive.not() -> showSnackBarError(R.string.alarm_error_activate)
            (LocationManager.getLastLatitude() + LocationManager.getLastLongitude()) == 0.toDouble() -> showSnackBarError(R.string.alarm_without_position)
            alertProvider.getMyAlertList().size > 20 -> showSnackBarError(R.string.alarm_limit)
            else -> {
                if (notificationId == COMPARTIR) {
                    shareLocation.onNext(Unit)
                } else {
                    val latitude = LocationManager.getLastLatitude()
                    val longitude = LocationManager.getLastLongitude()
                    getAddress(notificationId, latitude, longitude)
                }
            }
        }
    }

    private fun getAddress(notificationId: Int, latitude: Double, longitude: Double) {
        showLoading()
        disposables.add(geocodingController.getAddress(latitude, longitude)
                .subscribe({ hideLoading(); sendNotification(notificationId, it, latitude, longitude) }, { showServiceError(it) }))
    }

    private fun sendNotification(notificationId: Int, address: String, latitude: Double, longitude: Double) {
        showLoading()
        disposables.add(rastreoApiController.createNotification(notificationId, address, latitude, longitude)
                .subscribe({ hideLoading(); successAlarm.onNext(Unit) }, { showServiceError(it) }))
    }

    fun selectGroupNotifications() {
        myNotifications = false
        updateNotificationView()
    }

    fun selectMyNotifications() {
        myNotifications = true
        updateNotificationView()
    }

    fun getNotifications() {
        refreshing.set(true)
        disposables.add(rastreoApiController.notifications().subscribe(
                {
                    alertProvider.updateAlertList(it)
                    refreshing.set(false)
                },
                { refreshing.set(false);showServiceError(it) }))
    }

    fun alarmObservable(): Observable<Unit> =
            successAlarm.hide().observeOn(AndroidSchedulers.mainThread())

    fun shareLocationObservable(): Observable<Unit> =
            shareLocation.hide().observeOn(AndroidSchedulers.mainThread())

    fun observableNotificationsItems(): Observable<List<NotificationsModel>> =
            notificationsItem.observeOn(AndroidSchedulers.mainThread())

    fun getActions(): Observable<DashBoardUiModel> =
            viewModelActions.hide().observeOn(AndroidSchedulers.mainThread())

    sealed class DashBoardUiModel {
        class AnimationFloatButton(val visibility: Boolean) : DashBoardUiModel()
    }
}
