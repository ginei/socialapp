package com.social.app.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import com.social.base.utils.ErrorUtil
import com.social.app.R
import com.social.app.databinding.ActivitySharePositionBinding
import com.social.app.ui.BaseActivity
import com.social.app.ui.viewModels.SharePositionViewModel
import com.social.app.utils.CameraUtil
import com.social.base.utils.FileUtil

class SharePositionActivity : BaseActivity() {

    val binding: ActivitySharePositionBinding by lazy {
        DataBindingUtil.setContentView<ActivitySharePositionBinding>(this, R.layout.activity_share_position)
    }

    private val viewModel: SharePositionViewModel by lazy {
        SharePositionViewModel()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel
        setupActionBar(binding.toolbar)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun subscribe() {
        subscriptions.add(viewModel.getOpenCameraSubject().subscribe({

            CameraUtil.openCamera(this, FileUtil.getPathForUserFolder(), REQUEST_IMAGE_CAPTURE)
        }, { showError(binding.root, ErrorUtil.getMessageError(it)) }))

        subscriptions.add(viewModel.alarmObservable().subscribe { onBackPressed() })

        subscriptions.add(viewModel.observableSnackBar().subscribe { snackBarEvent -> showError(binding.root, snackBarEvent.message) })

        subscriptions.add(viewModel.observableShowLoading().subscribe { showLoading(it) })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                viewModel.loadImageFile()
            } else {
                showError(binding.root, getString(R.string.error_not_load_image))
            }
        }
        super.onActivityResult(requestCode, resultCode, intent)
    }

    override fun onResume() {
        super.onResume()
        subscribe()
    }

    companion object {
        private const val REQUEST_IMAGE_CAPTURE = 1
    }
}