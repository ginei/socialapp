package com.social.app.ui.adapters

import android.content.Context
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import com.social.base.utils.StringUtil
import com.social.app.R
import com.social.app.databinding.ViewOnboardingItemBinding
import com.social.app.ui.viewModels.OnBoardingViewModel

class CustomPagerAdapter(private val context: Context) : PagerAdapter() {

    var items: ArrayList<OnBoardingViewModel> = ArrayList()

    override fun instantiateItem(parent: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(context)
        val binding = DataBindingUtil.inflate<ViewOnboardingItemBinding>(inflater, R.layout.view_onboarding_item, parent, false)

        val onBoardingViewModel = items[position]

//        binding.imageView.setImageDrawable(onBoardingViewModel.drawable)
        binding.textViewTitle.text = onBoardingViewModel.title
        binding.textViewMessage.text = StringUtil.applySpans(onBoardingViewModel.message, RelativeSizeSpan(1.1f))

        parent.addView(binding.root)
        return binding.root
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getPageTitle(position: Int): CharSequence {
        return ""
    }

}