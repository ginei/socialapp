package com.social.app.ui.fragments;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.social.app.R;
import com.social.app.databinding.FragmentHomeBaseBinding;
import com.social.app.providers.NotificationProvider;
import com.social.app.ui.BaseFragment;
import com.social.app.ui.activities.MainActivity;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.subjects.BehaviorSubject;

public abstract class HomeBaseFragment extends BaseFragment {

    private FragmentHomeBaseBinding binding;

    private BehaviorSubject<NotificationProvider<Boolean>> switchState = BehaviorSubject.create();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home_base, container, false);

        View view = getView();
        if (view != null) {
            binding.layoutMainContainer.addView(view);
        }
        setListeners();

        return binding.getRoot();
    }

    private void setListeners() {
        binding.toolbar.setOnStateChangeListener(isCurrentStateActive -> switchState.onNext(new NotificationProvider<>(isCurrentStateActive)));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getContext() instanceof MainActivity) {
            ((MainActivity) getContext()).getObservableUserState().subscribe(aBoolean -> binding.toolbar.setStateActive(aBoolean));
        }
    }

    protected void userInactivity() {
        switchState.onNext(new NotificationProvider<>(new Throwable("User Inactivity")));
        binding.toolbar.setStateActive(false);
    }

    public Observable<NotificationProvider<Boolean>> getObservableSwitchState() {
        return switchState.observeOn(AndroidSchedulers.mainThread());
    }

    public abstract View getView();

}
