package com.social.app.di.component;

import com.social.app.di.scope.ActivityScope;
import com.social.app.ui.activities.MainViewModel;
import com.social.app.ui.alarmInProgress.AlarmInProgressViewModel;
import com.social.app.ui.login.LoginViewModel;
import com.social.app.ui.myNotifications.MyNotificationsViewModel;
import com.social.app.ui.sharePosition.SharePositionInProgressViewModel;
import com.social.app.ui.viewModels.AlarmViewModel;
import com.social.app.ui.viewModels.FooterViewModel;
import com.social.app.ui.viewModels.SharePositionViewModel;

import dagger.Component;

@ActivityScope
@Component(dependencies = AppComponent.class)
public interface ViewModelComponent extends AppComponent {

    void inject(LoginViewModel loginViewModel);

    void inject(FooterViewModel footerViewModel);

    void inject(AlarmViewModel alarmViewModel);

    void inject(AlarmInProgressViewModel alarmInProgressViewModel);

    void inject(SharePositionViewModel sharePositionViewModel);

    void inject(SharePositionInProgressViewModel sharePositionInProgressViewModel);

    void inject(MyNotificationsViewModel myNotificationsViewModel);

    void inject(MainViewModel mainViewModel);

}
