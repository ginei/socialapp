package com.social.app.di.component;

import com.social.app.di.scope.ActivityScope;
import com.social.app.others.ReplyBroadcastReceiver;

import dagger.Component;

@ActivityScope
@Component(dependencies = AppComponent.class)
public interface BroadcastComponent extends AppComponent {

    void inject(ReplyBroadcastReceiver replyBroadcastReceiver);

}
