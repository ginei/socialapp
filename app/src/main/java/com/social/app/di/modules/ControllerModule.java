package com.social.app.di.modules;

import com.social.app.api.controllers.DistancesController;
import com.social.app.api.controllers.GeocodingController;
import com.social.app.api.controllers.RastreoApiController;
import com.social.app.api.services.GeocodingApi;
import com.social.app.api.services.RastreoApi;
import com.social.app.api.services.platform.DistancesApi;
import com.social.app.providers.ResourceProvider;
import com.social.base.preferences.PrefsManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ControllerModule {

    @Provides
    @Singleton
    RastreoApiController rastreoApiController(RastreoApi rastreoApi, PrefsManager prefsManager) {
        return new RastreoApiController(rastreoApi, prefsManager);
    }

    @Provides
    @Singleton
    DistancesController distancesController(DistancesApi distancesApi) {
        return new DistancesController(distancesApi);
    }

    @Provides
    @Singleton
    GeocodingController geocodingController(GeocodingApi geocodingApi, ResourceProvider resourceProvider) {
        return new GeocodingController(geocodingApi, resourceProvider);
    }

}