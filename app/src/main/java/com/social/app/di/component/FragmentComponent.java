package com.social.app.di.component;

import com.social.app.di.scope.ActivityScope;
import com.social.app.ui.BaseFragment;
import com.social.app.ui.alarmInProgress.AlarmInProgressFragment;
import com.social.app.ui.fragments.DashboardFragment;
import com.social.app.ui.activities.SharePositionActivity;
import com.social.app.ui.sharePosition.SharePositionInProgressFragment;

import dagger.Component;

@ActivityScope
@Component(dependencies = AppComponent.class)
public interface FragmentComponent {

    void inject(BaseFragment baseFragment);

    void inject(DashboardFragment dashboardFragment);

    void inject(AlarmInProgressFragment alarmInProgressFragment);

    void inject(SharePositionActivity sharePositionActivity);

    void inject(SharePositionInProgressFragment sharePositionInProgressFragment);

}