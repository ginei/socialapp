package com.social.app.di.modules;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.social.base.preferences.PrefsManager;
import com.social.app.BuildConfig;
import com.social.app.R;
import com.social.app.api.config.ApiConfig;
import com.social.app.api.interceptors.ApplicationApiInterceptor;
import com.social.app.ui.GenericApplication;
import com.social.app.utils.GeneralUtils;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    private static final int TIME_OUT = 60;

    @Provides
    @Singleton
    public Retrofit retrofit(ApiConfig apiConfig, Gson gson, PrefsManager prefsManager) {

        OkHttpClient.Builder httpClient = getHttpClientBuilder(apiConfig, prefsManager);

        return getRetrofitBuilder(httpClient.build(), apiConfig.getBaseUrl(), gson).build();
    }

    @Provides
    @Singleton
    @Named("services")
    public Retrofit retrofitServices(ApiConfig apiConfig, Gson gson, PrefsManager prefsManager) {

        OkHttpClient.Builder httpClient = getHttpClientBuilder(apiConfig, prefsManager);

        return getRetrofitBuilder(httpClient.build(), apiConfig.getServicesUrl(), gson).build();
    }


    private OkHttpClient.Builder getHttpClientBuilder(ApiConfig apiConfig, PrefsManager prefsManager) {

        return getSimpleHttpClientBuilder(apiConfig)
                .addInterceptor(provideDynamicHeaderInterceptor(apiConfig, prefsManager));
    }


    private OkHttpClient.Builder getSimpleHttpClientBuilder(ApiConfig apiConfig) {

        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()

                .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(TIME_OUT, TimeUnit.SECONDS);

        if (apiConfig.DEBUG) {

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            clientBuilder.addInterceptor(logging);

            addInterceptors(apiConfig, clientBuilder);
        }

        return clientBuilder;

    }


    private void addInterceptors(ApiConfig apiConfig, OkHttpClient.Builder clientBuilder) {
        if (apiConfig.isMock) {
            clientBuilder.addInterceptor(new ApplicationApiInterceptor());
        }
    }

    private Retrofit.Builder getRetrofitBuilder(OkHttpClient httpClient, String url, Gson gson) {
        return new Retrofit.Builder()
                .client(httpClient)
                .baseUrl(url)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson));
    }

    @Provides
    @Singleton
    public Gson gson() {
        return new GsonBuilder()
                .create();
    }

    private Interceptor provideDynamicHeaderInterceptor(ApiConfig apiConfig, PrefsManager prefsManager) {

        Context context = GenericApplication.get();

        String androidID = GeneralUtils.getAndroidID(context);

        return chain -> {
            Request.Builder newBuilder;

            Request finalRequest = chain.request();
            newBuilder = getDefaultHeaders(androidID, chain);

            if (finalRequest.url().url().toString().contains(apiConfig.getBaseUrl())) {
                String user = GenericApplication.get().getString(R.string.user) + ":" + GenericApplication.get().getString(R.string.password);

                String auth = "Basic " + "YWRtaW46cHRwc29sdXRpb25zMTc=";/*Base64.encode(user.getBytes(), Base64.NO_WRAP);*/
                newBuilder.header(ApiConfig.AUTHORIZATION, auth);
            }
            if (!finalRequest.method().equalsIgnoreCase("GET")) {
                newBuilder.header(ApiConfig.CONTENT_TYPE, ApiConfig.APPLICATION_JSON);
            } else if (finalRequest.method().equalsIgnoreCase("GET")) {
                newBuilder.header(ApiConfig.ACCEPT, ApiConfig.APPLICATION_JSON);
            }
            finalRequest = newBuilder.build();
            return chain.proceed(finalRequest);
        };
    }

    private Request.Builder getDefaultHeaders(String androidID, Interceptor.Chain chain) {
        return chain.request().newBuilder()
                .header(ApiConfig.DEVICE_ID, androidID)
                .header(ApiConfig.APP_VERSION, String.valueOf(BuildConfig.VERSION_CODE))
                .method(chain.request().method(), chain.request().body());
    }
}
