package com.social.app.di.modules

import com.social.app.providers.AlertProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ProviderModule {

    @Provides
    @Singleton
    fun alertProvider(): AlertProvider {
        return AlertProvider()
    }
}