package com.social.app.di.component;

import com.social.app.di.scope.ActivityScope;
import com.social.app.ui.BaseActivity;
import com.social.app.ui.activities.MainActivity;
import com.social.app.ui.myNotifications.MyNotificationsActivity;
import com.social.app.ui.pin.PinVerificationActivity;
import com.social.app.ui.splash.SplashActivity;

import dagger.Component;

@ActivityScope
@Component(dependencies = AppComponent.class)
public interface ActivityComponent extends AppComponent {

    void inject(BaseActivity baseActivity);

    void inject(MainActivity mainActivity);

    void inject(MyNotificationsActivity myNotificationsActivity);

    void inject(PinVerificationActivity pinVerificationActivity);

    void inject(SplashActivity splashActivity);

}
