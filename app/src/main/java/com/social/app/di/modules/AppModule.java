package com.social.app.di.modules;

import android.content.Context;

import com.social.base.eventbus.RxBus;
import com.social.base.preferences.PrefsManager;
import com.social.app.api.config.ApiConfig;
import com.social.app.providers.ResourceProvider;
import com.social.app.providers.ToastManager;
import com.social.app.ui.GenericApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class AppModule {

    private final GenericApplication app;

    public AppModule(GenericApplication app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public Context appContext() {
        return app;
    }

    @Provides
    @Singleton
    public PrefsManager preferenceManager(Context context) {
        PrefsManager.init(context);
        return PrefsManager.getInstance();
    }

    @Provides
    @Singleton
    public ApiConfig getApiConfig(Context context, PrefsManager prefsManager) {
        return new ApiConfig(context, prefsManager);
    }

    @Provides
    @Singleton
    public ResourceProvider resourceProvider(Context context) {
        return new ResourceProvider(context);
    }

    @Provides
    @Singleton
    public ToastManager toastManager(Context context) {
        return new ToastManager(context);
    }

    @Provides
    @Singleton
    public RxBus rxBus() {
        return RxBus.getInstance();
    }

}
