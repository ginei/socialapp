package com.social.app.di.component;

import android.content.Context;

import com.social.base.eventbus.RxBus;
import com.social.base.preferences.PrefsManager;
import com.social.app.api.config.ApiConfig;
import com.social.app.api.controllers.DistancesController;
import com.social.app.api.controllers.GeocodingController;
import com.social.app.api.controllers.RastreoApiController;
import com.social.app.di.modules.Api;
import com.social.app.di.modules.ApiModule;
import com.social.app.di.modules.AppModule;
import com.social.app.di.modules.ControllerModule;
import com.social.app.di.modules.ProviderModule;
import com.social.app.providers.AlertProvider;
import com.social.app.providers.ResourceProvider;
import com.social.app.providers.ToastManager;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {AppModule.class, ApiModule.class, ControllerModule.class, Api.class, ProviderModule.class})
public interface AppComponent {

    PrefsManager preferenceManager();

    ToastManager toastProvider();

    RxBus rxBus();

    Context context();

    /**
     * Apis
     **/

    ApiConfig getApiConfig();

    Retrofit retrofit();

    /**
     * Controllers
     */
    RastreoApiController rastreoApiController();

    DistancesController distancesController();

    GeocodingController geocodingController();


    /**
     * Providers
     */

    ResourceProvider resourceProvider();

    AlertProvider alertProvider();
}