package com.social.app.di.modules

import com.social.app.api.services.GeocodingApi
import com.social.app.api.services.RastreoApi
import com.social.app.api.services.platform.DistancesApi
import com.social.app.api.services.platform.LocationApi

import javax.inject.Named
import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class Api {

    @Provides
    @Singleton
    fun AuthenticatorApi(retrofit: Retrofit): LocationApi {
        return retrofit.create(LocationApi::class.java)
    }

    @Provides
    @Singleton
    fun distancesApi(retrofit: Retrofit): DistancesApi {
        return retrofit.create(DistancesApi::class.java)
    }

    @Provides
    @Singleton
    fun servicesApi(@Named("services") retrofit: Retrofit): RastreoApi {
        return retrofit.create(RastreoApi::class.java)
    }

    @Provides
    @Singleton
    fun geocodingApi(@Named("services") retrofit: Retrofit): GeocodingApi {
        return retrofit.create(GeocodingApi::class.java)
    }

}
