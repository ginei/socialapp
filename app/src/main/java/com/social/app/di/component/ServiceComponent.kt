package com.social.app.di.component

import com.social.app.di.scope.ActivityScope
import com.social.app.services.BluetoothLEService
import com.social.app.services.GeneralFirebaseMessagingService
import com.social.app.services.LocationServiceApi
import dagger.Component

@ActivityScope
@Component(dependencies = [AppComponent::class])
interface ServiceComponent : AppComponent {

    fun inject(locationServiceApi: LocationServiceApi)

    fun inject(generalFirebaseMessagingService: GeneralFirebaseMessagingService)

    fun inject(bluetoothLEService: BluetoothLEService)

}
