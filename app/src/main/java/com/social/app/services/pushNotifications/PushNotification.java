package com.social.app.services.pushNotifications;

public class PushNotification {

    public int id;
    public int icon;

    public int sound;
    public String message;
    public String title;
    public String typeSpecial;
}