package com.social.app.services.pushNotifications

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.RemoteInput
import androidx.core.app.TaskStackBuilder
import androidx.core.content.ContextCompat
import com.social.base.utils.AndroidVersionUtil
import com.social.app.R
import com.social.app.managers.HandlerNotificationsManager
import com.social.app.others.ReplyBroadcastReceiver
import com.social.app.ui.splash.SplashActivity
import java.util.*

const val CHANNEL_ID = "notifications"

class PushNotificationBuilder {


    fun show(context: Context, pushNotification: PushNotification) {
        val classToStart = SplashActivity::class.java
        val resultIntent = Intent(context, classToStart)
        val stackBuilder = TaskStackBuilder.create(context)
        stackBuilder.addParentStack(classToStart)
        stackBuilder.addNextIntent(resultIntent)
        val pendingContentIntent = stackBuilder.getPendingIntent(pushNotification.id, PendingIntent.FLAG_UPDATE_CURRENT)
        val builder = NotificationCompat.Builder(context)
                .setSmallIcon(pushNotification.icon)
                .setContentTitle(pushNotification.title)
                .setContentText(pushNotification.message)
                .setAutoCancel(true)
                .setContentIntent(pendingContentIntent)
                .setSound(Uri.parse(context.getString(R.string.uri_package_sound, context.packageName, pushNotification.sound)))
                .setPriority(Notification.PRIORITY_HIGH)
                .setStyle(NotificationCompat.BigTextStyle()
                        .bigText(pushNotification.message))
                .setColor(ContextCompat.getColor(context, R.color.red))
        HandlerNotificationsManager.createNotification(pushNotification.id, builder.build())
    }

    private fun prepareReplyActionNotification(context: Context, pushNotification: PushNotification,
                                               builder: NotificationCompat.Builder): NotificationCompat.Builder {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val replyIntent = PendingIntent.getBroadcast(context,
                    pushNotification.id, getMessageReplyIntent(context, 1), PendingIntent.FLAG_UPDATE_CURRENT)
            val replyLabel = context.getString(R.string.app_name)
            val remoteInput = RemoteInput.Builder(ReplyBroadcastReceiver.KEY_TEXT_REPLY)
                    .setLabel(replyLabel)
                    .build()
            val replyAction = NotificationCompat.Action.Builder(R.drawable.ic_send, replyLabel, replyIntent)
                    .addRemoteInput(remoteInput)
                    .build()
            builder.addAction(replyAction)
                    .setSound(Uri.parse(context.getString(R.string.uri_package_sound, context.packageName, pushNotification.sound)))
        }
        return builder
    }

    private fun getMessageReplyIntent(context: Context, conversationId: Int): Intent {
        return Intent(context, ReplyBroadcastReceiver::class.java)
                .addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
                .setAction(REPLY_ACTION)
                .putExtra(NOTIFICATION_ID, conversationId)
    }

    fun showSpecialNotification(context: Context, incomingNotification: PushNotification) {
        if (AndroidVersionUtil.hasOreo())
            createNotificationChannel(context, CHANNEL_ID, "Notification")

        val builder: NotificationCompat.Builder
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val activeNotifications = HandlerNotificationsManager.getActiveNotifications()
            val stackNotifications = ArrayList<CharSequence>()
            for (statusBarNotification in activeNotifications) {
                if (statusBarNotification.id == 1 + incomingNotification.typeSpecial.hashCode()) {
                    val notification = statusBarNotification.notification
                    val group = notification.group
                    if (group != null && group == GROUP_KEY_SPECIAL) {
                        val textLines = notification.extras.get(NotificationCompat.EXTRA_TEXT_LINES) as Array<CharSequence>
                        if (textLines == null) {
                            stackNotifications.add(notification.extras.get(NotificationCompat.EXTRA_TEXT) as String)
                        } else {
                            Collections.addAll(stackNotifications, *textLines)
                        }
                    }
                }
            }
            stackNotifications.add(incomingNotification.message)
            builder = createSummaryBuilder(context, stackNotifications, incomingNotification)
        } else {
            builder = createBasicBuilder(context, incomingNotification).setGroup(GROUP_KEY_SPECIAL)
        }
        HandlerNotificationsManager.createNotification(1, builder.build())
    }

    private fun createBasicBuilder(context: Context, pushNotification: PushNotification): NotificationCompat.Builder {

        val contentIntent = Intent(context, SplashActivity::class.java)
        contentIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val pendingContentIntent = TaskStackBuilder.create(context)
                .addNextIntentWithParentStack(contentIntent)
                .getPendingIntent(pushNotification.id, PendingIntent.FLAG_UPDATE_CURRENT)

        return NotificationCompat.Builder(context)
                .setSmallIcon(pushNotification.icon)
                .setContentTitle(pushNotification.title)
                .setContentText(pushNotification.message)
                .setContentIntent(pendingContentIntent)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setStyle(NotificationCompat.BigTextStyle().bigText(pushNotification.message))
                .setSound(Uri.parse(context.getString(R.string.uri_package_sound, context.packageName, pushNotification.sound)))
                .setColor(ContextCompat.getColor(context, R.color.red))
    }

    private fun createSummaryBuilder(context: Context, notifications: List<CharSequence>, pushNotification: PushNotification): NotificationCompat.Builder {
        val contentIntent = Intent(context, SplashActivity::class.java)
        contentIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val pendingContentIntent = PendingIntent.getActivity(context, 1597, contentIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val inboxStyle = NotificationCompat.InboxStyle()
        var i = 0
        val limit = notifications.size
        while (i < limit) {
            inboxStyle.addLine(notifications[i])
            i++
        }
        val summaryText = context.getString(R.string.push_new_message)
        inboxStyle.setSummaryText(summaryText)
        val largeIcon = BitmapFactory.decodeResource(context.resources, R.drawable.logo_social)
        return NotificationCompat.Builder(context)
                .setContentTitle(pushNotification.title)
                .setContentText(summaryText)
                .setSmallIcon(R.drawable.logo_social)
                .setLargeIcon(largeIcon)
                .setContentIntent(pendingContentIntent)
                .setStyle(inboxStyle)
                .setAutoCancel(true)
                .setNumber(notifications.size)
                .setGroup(GROUP_KEY_SPECIAL)
                .setGroupSummary(true)
                .setSound(Uri.parse(context.getString(R.string.uri_package_sound, context.packageName, pushNotification.sound)))
                .setColor(ContextCompat.getColor(context, R.color.red))
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(context: Context, channelId: String, channelName: String) {
        val chan = NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_NONE)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
    }

    companion object {

        private val REPLY_ACTION = "com.social.app.handlers.push.PushNotificationBuilder.ACTION_MESSAGE_REPLY"
        val NOTIFICATION_ID = "notification_id"

        val SPECIAL_PUSH_ID = 1001
        val DEFAULT_PUSH_ID = 1002

        private val GROUP_KEY_SPECIAL = "group_key_special"
    }
}
