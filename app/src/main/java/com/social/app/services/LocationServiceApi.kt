package com.social.app.services

import android.Manifest
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.social.app.R
import com.social.app.api.controllers.RastreoApiController
import com.social.app.managers.HandlerNotificationsManager
import com.social.app.managers.LocationManager
import com.social.app.services.pushNotifications.CHANNEL_ID
import com.social.base.eventbus.RxBus
import com.social.base.utils.AndroidVersionUtil
import com.social.base.utils.LogUtil
import javax.inject.Inject

class LocationServiceApi : BaseService(), GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private val locationInterval = intArrayOf(50000, 10000)
    private val fastestLocationInterval = intArrayOf(40000, 10000)
    private val MIN_DISPLACEMENT = 100

    private val TAG = this.javaClass.name

    private var googleApiClient: GoogleApiClient? = null

    private var locationRequest: LocationRequest? = null

    @Inject
    lateinit var rastreoApiController: RastreoApiController

    @Inject
    lateinit var rxBus: RxBus

    private val pendingIntentForNotification: PendingIntent
        get() = PendingIntent.getService(baseContext, 0, Intent(baseContext, LocationServiceApi::class.java), PendingIntent.FLAG_UPDATE_CURRENT)


    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    override fun onCreate() {
        component.inject(this)
        super.onCreate()
        startTracking()
        if (AndroidVersionUtil.hasOreo())
            createNotificationChannel(CHANNEL_ID, "Notification")
        startForeground(NOTIFICATION_ID, HandlerNotificationsManager.getNotificationForService(this,
                getString(R.string.app_name), getString(R.string.notification_running), pendingIntentForNotification, CHANNEL_ID))
    }

    private fun startTracking() {

        if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {

            googleApiClient = GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build()

            if (googleApiClient?.isConnected?.not() == true || googleApiClient?.isConnecting?.not() == true) {
                googleApiClient?.connect()
            }
        } else {
            LogUtil.e(this.javaClass.name, "unable to connect to google play services.")
        }
    }

    override fun onConnected(bundle: Bundle?) {
        locationRequest = LocationRequest.create()
        locationRequest!!.interval = locationInterval[0].toLong()
        locationRequest!!.fastestInterval = fastestLocationInterval[0].toLong()
        locationRequest!!.smallestDisplacement = MIN_DISPLACEMENT.toFloat()
        locationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        startLocationUpdates()
    }

    private fun startLocationUpdates() {
        if (!validateLocationPermissions()) {
            LocationManager.setGoogleApiClient(googleApiClient)
            LocationManager.setLocationRequest(locationRequest)
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        } else {
            LogUtil.e(TAG, "Permission denied")
        }
    }

    private fun validateLocationPermissions(): Boolean {
        return ActivityCompat.checkSelfPermission(baseContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(baseContext,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
    }

    override fun onConnectionSuspended(i: Int) {

    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }

    override fun onLocationChanged(location: Location) {
        LocationManager.setLocation(location)
    }

    override fun onDestroy() {
        if (googleApiClient != null && googleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this)
            stopForeground(true)
            googleApiClient!!.disconnect()
        }
        super.onDestroy()
    }

    companion object {

        val NOTIFICATION_ID = 1
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String) {
        val chan = NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_NONE)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
    }
}