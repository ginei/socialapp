package com.social.app.services

import android.app.Service


import com.social.app.di.component.DaggerServiceComponent
import com.social.app.di.component.ServiceComponent
import com.social.app.ui.GenericApplication

import io.reactivex.disposables.CompositeDisposable

abstract class BaseService : Service() {

     val disposables: CompositeDisposable by lazy {
        CompositeDisposable()
    }

    protected val component: ServiceComponent
        get() = DaggerServiceComponent.builder()
                .appComponent(GenericApplication.get().getAppComponent())
                .build()

    override fun onDestroy() {
        this.disposables.clear()
        super.onDestroy()
    }
}
