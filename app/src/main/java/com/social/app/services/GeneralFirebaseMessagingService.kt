package com.social.app.services


import android.content.Intent
import android.text.TextUtils
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.social.base.eventbus.RxBus
import com.social.base.preferences.PrefsManager
import com.social.base.utils.StringUtil
import com.social.app.R
import com.social.app.di.component.DaggerServiceComponent
import com.social.app.di.component.ServiceComponent
import com.social.app.events.ActivateServiceEvent
import com.social.app.events.NotificationEvent
import com.social.app.managers.ProfileManager
import com.social.app.managers.preferences.ConstantPreferences
import com.social.app.services.pushNotifications.PushNotification
import com.social.app.services.pushNotifications.PushNotificationBuilder
import com.social.app.ui.GenericApplication
import com.social.app.ui.splash.SplashActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

open class GeneralFirebaseMessagingService : FirebaseMessagingService() {

    @Inject
    lateinit var rxBus: RxBus

    protected val component: ServiceComponent
        get() = DaggerServiceComponent.builder()
                .appComponent(GenericApplication.get().getAppComponent())
                .build()

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)
        component.inject(this)
        val map = remoteMessage!!.data

        when {
            map.containsKey(PUSH_TYPE) -> handleTypedPushNotifications(map[PUSH_TYPE] ?: "", map)
            remoteMessage.notification != null -> handleGenericNotification(remoteMessage.notification)
            else -> handlerPushNotification(map)
        }
    }

    private fun handleTypedPushNotifications(pushType: String, data: MutableMap<String, String>) {
        if (pushType.equals(PANIC, ignoreCase = true) || pushType.equals(SHARE_LOCATION, ignoreCase = true)) {
            handlePanic(pushType, data)
        } else if (pushType.equals(ACTIVATE_SERVICE, ignoreCase = true)) {
            setActivateService(data[STATE] == "true")
        } else if (pushType.equals(OPEN_APP, ignoreCase = true)) {
            openApp()
        } else {
            handleDefaultPush(data)
        }
    }

    private fun openApp() {
        applicationContext.startActivity(Intent(applicationContext, SplashActivity::class.java))
    }

    private fun setActivateService(state: Boolean) {
        if (state) {
//            ServiceUtils.restartService(LocationServiceApi::class.java)
        } else {
//            ServiceUtils.stopService(LocationServiceApi::class.java)
        }

        Observable.timer(3000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    rxBus.post(ActivateServiceEvent(state))
                    PrefsManager.getInstance().set(ConstantPreferences.IS_ACTIVATE, state)
                }
    }

    private fun getTone(pushType: String): String {
        return if (pushType.equals(SHARE_LOCATION, ignoreCase = true)) {
            "tone"
        } else "tone"
    }

    private fun handlePanic(pushType: String, data: Map<String, String>) {
        var userId = "-1"
        try {
            userId = ProfileManager.userId.toString()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (userId != "0" && data[USER_ID] != userId) {
            val pushNotification = PushNotification()
            pushNotification.title = data["title"]
            pushNotification.message = data["alert"]
            pushNotification.icon = R.drawable.logo_social
            pushNotification.sound = resources.getIdentifier(getTone(pushType), "raw", packageName)
            pushNotification.id = PushNotificationBuilder.SPECIAL_PUSH_ID
            pushNotification.typeSpecial = "1"

            val pushNotificationBuilder = PushNotificationBuilder()
            pushNotificationBuilder.showSpecialNotification(applicationContext, pushNotification)
            try {
                rxBus.post(NotificationEvent(true))
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            try {
                rxBus.post(NotificationEvent())
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    private fun handleDefaultPush(data: MutableMap<String, String>) {
        data["sound"] = "tone.mp3"
        defaultPush(data)
    }

    private fun handleGenericNotification(notification: RemoteMessage.Notification?) {
        if (notification != null) {
            val pushNotification = PushNotification()
            pushNotification.title = if (TextUtils.isEmpty(notification.title))
                getString(R.string.app_name)
            else
                notification.title
            pushNotification.message = notification.body
            pushNotification.icon = R.drawable.logo_social
            pushNotification.id = PushNotificationBuilder.SPECIAL_PUSH_ID
            val pushNotificationBuilder = PushNotificationBuilder()
            pushNotificationBuilder.show(applicationContext, pushNotification)
        }
    }

    private fun handlerPushNotification(map: Map<String, String>) {
        defaultPush(map)
    }

    private fun defaultPush(data: Map<String, String>?) {
        try {
            val pushNotification = PushNotification()
            if (data != null) {
                pushNotification.title = data["title"]
                pushNotification.message = data["alert"]
            } else {
                pushNotification.title = getString(R.string.notification_push)
                pushNotification.message = getString(R.string.notification_new_push)
            }
            pushNotification.icon = R.drawable.logo_social
            pushNotification.id = PushNotificationBuilder.DEFAULT_PUSH_ID
            if (data != null) {
                val sound = data["sound"]
                if (sound != null && !sound.equals("null", ignoreCase = true)) {
                    if (sound.split("\\\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray().size > 1) {
                        pushNotification.sound = resources.getIdentifier(sound.split("\\\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0], "raw", packageName)
                    } else if (sound.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray().size > 1) {
                        pushNotification.sound = resources.getIdentifier(sound.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0], "raw", packageName)
                    } else if (sound.split(".".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray().size > 1) {
                        pushNotification.sound = resources.getIdentifier(sound.split(".".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0], "raw", packageName)
                    }
                }
            }
            if (!StringUtil.isEmpty(pushNotification.title) && !StringUtil.isEmpty(pushNotification.message)) {
                val pushNotificationBuilder = PushNotificationBuilder()
                pushNotificationBuilder.show(applicationContext, pushNotification)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    companion object {

        const val PUSH_TYPE = "type"

        private const val PANIC = "panic"
        private const val USER_ID = "userId"
        private const val STATE = "state"
        private const val ACTIVATE_SERVICE = "ACTIVATE_SERVICE"
        private const val OPEN_APP = "OPEN_APP"
        private const val SHARE_LOCATION = "shareLocation"
    }
}
