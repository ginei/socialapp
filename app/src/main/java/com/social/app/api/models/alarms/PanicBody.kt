package com.social.app.api.models.alarms

import com.google.gson.annotations.SerializedName

data class PanicBody(

        @SerializedName("id_user")
        private val idUser: String? = null,

        @SerializedName("latitude")
        private val latitude: Double? = null,

        @SerializedName("longitude")
        private val longitude: Double? = null,

        @SerializedName("id_notificacion")
        private val idNotification: Int = 0,

        @SerializedName("grupo")
        private val group: Int? = null,

        @SerializedName("names")
        private val names: String? = null,

        @SerializedName("telephone")
        private val telephone: String? = null,

        @SerializedName("uuid")
        private val uuid: String? = null,

        @SerializedName("address")
        private val address: String? = null,

        @SerializedName("comment")
        private val description: String? = null,

        @SerializedName("url_photo")
        private val urlPhotoFirebase: String? = null,

        @SerializedName("visibility_system")
        private val visibilitySystem: Boolean? = null

)