package com.social.app.api.controllers

import com.social.app.R
import com.social.app.api.models.geocoding.Address
import com.social.app.api.models.geocoding.AddressModel
import com.social.app.api.services.GeocodingApi
import com.social.app.managers.LocationManager
import com.social.app.providers.ResourceProvider
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GeocodingController(private val geocodingApi: GeocodingApi, private val resourceProvider: ResourceProvider) {

    fun getAddress(latitude: Double = LocationManager.getLastLatitude(), longitude: Double = LocationManager.getLastLongitude()): Single<String> {
        return getAddressGoogle(latitude, longitude)
                .flatMap { successAddressGoogle(latitude, longitude, it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    private fun getAddressOsm(latitude: Double, longitude: Double): Single<String> =
            geocodingApi.getAddress("json", 18, latitude, longitude)
                    .flatMap { formatAddressOsm(it.address) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())


    private fun getAddressGoogle(latitude: Double, longitude: Double): Single<AddressModel> =
            geocodingApi.getAddressGoogle(latitude.toString() + "," + longitude.toString(), resourceProvider.getString(R.string.google_geocoding_key))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    private fun successAddressGoogle(latitude: Double, longitude: Double, it: AddressModel?): Single<String> {
        it?.googleAddress?.firstOrNull()?.formattedAddress?.let {
            return Single.just(it)
        } ?: return getAddressOsm(latitude, longitude)
    }

    private fun formatAddressOsm(address: Address?): Single<String> {
        var addressString = ""

        address?.let {
            it.road?.let { addressString += it + ", " }
            it.neighbourhood?.let { addressString += it + ", " }
            it.suburb?.let { addressString += it + ", " }
            it.city?.let { addressString += it + ", " }
            it.state?.let { addressString += it + ", " }
            it.country?.let { addressString += it + ", " }
        }
        return Single.just(addressString)
    }

}
