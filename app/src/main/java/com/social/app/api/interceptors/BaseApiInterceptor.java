package com.social.app.api.interceptors;

import com.social.app.api.config.ApiConfig;
import com.social.app.utils.GeneralUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;


public abstract class BaseApiInterceptor implements Interceptor {

    Response buildResponseFromJsonString(Chain chain, String jsonString) {
        return new Response.Builder()
                .code(200)
                .sentRequestAtMillis(3000)
                .request(chain.request())
                .message("OK")
                .protocol(Protocol.HTTP_2)
                .body(ResponseBody.create(MediaType.parse(ApiConfig.APPLICATION_JSON), jsonString))
                .build();
    }


    public Response getResponse(Chain chain, String nameFileJson) {
        return new Response.Builder()
                .code(200)
                .request(chain.request())
                .message("OK")
                .sentRequestAtMillis(3000)
                .protocol(Protocol.HTTP_2)
                .body(ResponseBody.create(MediaType.parse(ApiConfig.APPLICATION_JSON), GeneralUtils.loadJSONFromAsset(nameFileJson).getBytes()))
                .build();
    }

    public Response getResponseForwad(Chain chain) throws IOException {
        return new Response.Builder()
                .code(200)
                .request(chain.request())
                .message("OK")
                .protocol(Protocol.HTTP_2)
                .body(ResponseBody.create(MediaType.parse(ApiConfig.APPLICATION_JSON), bodyToString(chain.request())))
                .build();
    }


    public String getPath(Chain chain) {
        return chain.request().url().uri().getPath();
    }

    static String bodyToString(final Request request) throws IOException {

        final Request copy = request.newBuilder().build();
        final Buffer buffer = new Buffer();
        copy.body().writeTo(buffer);
        return buffer.readUtf8();

    }
}
