package com.social.app.api.models

import com.google.gson.annotations.SerializedName

data class Position(

        @SerializedName("deviceId")
        var deviceId: String,

        @SerializedName("latitude")
        var latitude: Double,

        @SerializedName("longitude")
        var longitude: Double,

        @SerializedName("deviceTime")
        var deviceTime: String

)
