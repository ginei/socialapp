package com.social.app.api.models.alarms

import com.google.gson.annotations.SerializedName

data class NotificationRequest(

        @SerializedName("hour")
        private val hour: Int = 5
)
