package com.social.app.api.models

import com.google.gson.annotations.SerializedName

data class LoginModel(

        @SerializedName("telephone")
        var telephone: String? = null

)
