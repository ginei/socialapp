package com.social.app.api.models


import com.google.gson.annotations.SerializedName

class ProfileResponse {

    @SerializedName("id")
    var id: String = "0"

    @SerializedName("groupId")
    var group: Int? = 0

    @SerializedName("first_name")
    val firstName: String? = null

    @SerializedName("last_name")
    val lastName: String? = null

    @SerializedName("telephone")
    var phone: String? = null

    @SerializedName("email")
    val email: String? = null

    @SerializedName("state")
    val state: Int = 0

    @SerializedName("created_at")
    val createdAt: String? = null

    @SerializedName("profile_pic")
    val profilePic: String? = null

    @SerializedName("type")
    val type: String? = null

    @SerializedName("is_active")
    val isActive: Boolean = false

    @SerializedName("birthday")
    val birthday: String? = null

    @SerializedName("country")
    val country: String? = null

    @SerializedName("city")
    val city: String? = null

    @SerializedName("gender")
    val gender: String? = null

    @SerializedName("name")
    var name: String? = null
}
