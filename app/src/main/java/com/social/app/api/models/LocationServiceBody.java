package com.social.app.api.models;


import com.google.gson.annotations.SerializedName;

public class LocationServiceBody {

    @SerializedName("id")
    private Long id;

    @SerializedName("lat")
    private Double latitude;

    @SerializedName("lng")
    private Double longitude;

    @SerializedName("precision")
    private Float accuracy;

    @SerializedName("bearing")
    private Float bearing;

    public LocationServiceBody(Double latitude, Double longitude, Float accuracy, Float bearing) {
        this(latitude, longitude, accuracy);
        this.bearing = bearing;
    }

    public LocationServiceBody(Double latitude, Double longitude, Float accuracy) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.accuracy = accuracy;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Float accuracy) {
        this.accuracy = accuracy;
    }

    public Float getBearing() {
        return bearing;
    }
}
