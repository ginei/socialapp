package com.social.app.api.interceptors

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class ApplicationApiInterceptor : BaseApiInterceptor() {

    private val LOGIN = "/device/3153249283"

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val path = getPath(chain)


        return when (path) {
            LOGIN -> {
                getResponse(chain, "mockServices/login.json")
            }
            else -> chain.proceed(chain.request())
        }
    }

    private fun getNameFile(url: String): String {
        val folder = "mockServices/"
        return folder + url.substring(url.lastIndexOf("/") + 1, url.length) + ".json"
    }

}