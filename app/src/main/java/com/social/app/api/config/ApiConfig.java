package com.social.app.api.config;

import android.content.Context;
import android.text.TextUtils;
import com.social.base.preferences.PrefsManager;
import com.social.app.R;
import com.social.app.managers.preferences.ConstantPreferences;

public class ApiConfig {

    public static final String AUTHORIZATION = "Authorization";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String ACCEPT = "Accept";
    public static final String APPLICATION_JSON = "application/json";
    public static final String DEVICE_ID = "DeviceID";
    public static final String APP_VERSION = "appVersion";
    public static final String MULTI_PART = "multipart/form-data";

    public final boolean DEBUG = true;
    public final boolean isMock = false;

    private Context context;

    private PrefsManager prefsManager;
    private String resourceUrl;


    public ApiConfig(Context context, PrefsManager prefsManager) {
        this.context = context;
        this.prefsManager = prefsManager;
        updateUrl(context.getString(R.string.url_services));
    }

    private void updateUrl(String resourceUrl) {
        String preferenceUrl = prefsManager.getString(ConstantPreferences.BASE_URL);
        if (TextUtils.isEmpty(preferenceUrl)) {
            this.resourceUrl = resourceUrl;
        }
    }

    public String getBaseUrl() {
        String prefUrl = prefsManager.getString(ConstantPreferences.BASE_URL);
        if (TextUtils.isEmpty(prefUrl)) {
            return resourceUrl;
        }
        return prefUrl;
    }

    public String getServicesUrl() {
        return context.getString(R.string.url_services);
    }

}