package com.social.app.api.models.geocoding

import com.google.gson.annotations.SerializedName

data class AddressModel(

        @SerializedName("address")
        var address: Address? = null,

        @SerializedName("results")
        var googleAddress: List<Address>? = null

)

data class Address(
        @SerializedName("road")
        var road: String? = null,

        @SerializedName("neighbourhood")
        var neighbourhood: String? = null,

        @SerializedName("suburb")
        var suburb: String? = null,

        @SerializedName("county")
        var city: String? = null,

        @SerializedName("state")
        var state: String? = null,

        @SerializedName("country")
        var country: String? = null,

        @SerializedName("formatted_address")
        var formattedAddress: String? = null
)
