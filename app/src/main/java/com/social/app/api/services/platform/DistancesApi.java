package com.social.app.api.services.platform;

import com.social.app.api.models.Position;
import com.social.app.api.models.Summary;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DistancesApi {

    @GET("api/reports/summary")
    Single<List<Summary>> getSummary(@Query("from") String from, @Query("to") String to, @Query("deviceId") String deviceId);

    @GET("api/positions")
    Observable<List<Position>> getLastPositionById();

}
