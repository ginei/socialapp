package com.social.app.api.controllers;

import android.util.Log;

import com.social.base.utils.DateUtils;
import com.social.app.api.models.Position;
import com.social.app.api.models.Summary;
import com.social.app.api.services.platform.DistancesApi;
import com.social.app.managers.ProfileManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class DistancesController {

    private DistancesApi distancesApi;

    public DistancesController(DistancesApi distancesApi) {
        this.distancesApi = distancesApi;
    }

    private Single<List<Summary>> getSummary(String start, String end) {
        return distancesApi.getSummary(start, end,  ProfileManager.INSTANCE.getUserId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Position> getLastPositionById(String userId) {

        return distancesApi.getLastPositionById()
                .flatMap(positions -> getPositionByID(userId, positions))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<Position> getPositionByID(String userId, List<Position> positions) {

        for (Position position : positions) {
            if (position.getDeviceId() == userId) {
                return Observable.just(position);
            }
        }
        return Observable.error(new Throwable("Current position not found"));
    }

    public Single<List<Summary>> getSummaryWeek(String start, String end) {

        Log.e("Time", "" + new SimpleDateFormat("Z", Locale.getDefault()).format(System.currentTimeMillis()));
        Log.e("Time", "start " + start);
        Log.e("Time ", "end " + end);
        Log.e("Time", "" + getDate(start, DateUtils.DEFAULT_FORMAT_DATE));

        return Single.zip(
                getSummary(start, DateUtils.getDateWithAdditionalDay(start, 1)),
                getSummary(DateUtils.getDateWithAdditionalDay(start, 1), DateUtils.getDateWithAdditionalDay(start, 2)),
                getSummary(DateUtils.getDateWithAdditionalDay(start, 2), DateUtils.getDateWithAdditionalDay(start, 3)),
                getSummary(DateUtils.getDateWithAdditionalDay(start, 3), DateUtils.getDateWithAdditionalDay(start, 4)),
                getSummary(DateUtils.getDateWithAdditionalDay(start, 4), DateUtils.getDateWithAdditionalDay(start, 5)),
                getSummary(DateUtils.getDateWithAdditionalDay(start, 5), DateUtils.getDateWithAdditionalDay(start, 6)),
                getSummary(DateUtils.getDateWithAdditionalDay(start, 6), DateUtils.getDateWithAdditionalDay(start, 7)),
                (value1, value2, value3, value4, value5, value6, value7) -> {

                    List<Summary> positions = new ArrayList<>();
                    positions = addPosition(positions, value1);
                    positions = addPosition(positions, value2);
                    positions = addPosition(positions, value3);
                    positions = addPosition(positions, value4);
                    positions = addPosition(positions, value5);
                    positions = addPosition(positions, value6);
                    positions = addPosition(positions, value7);

                    return positions;
                }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private List<Summary> addPosition(List<Summary> positions, List<Summary> values) {
        positions.addAll(values);
        return positions;
    }

    private static String getDate(String dateInString, String format) {
        String date = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date d = dateFormat.parse(dateInString);
            date = DateUtils.parseDateToStringWithFormat(d, DateUtils.ISO_8601);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

}
