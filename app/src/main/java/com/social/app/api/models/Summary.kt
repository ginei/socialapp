package com.social.app.api.models

import com.google.gson.annotations.SerializedName

data class Summary(

        @SerializedName("deviceName")
        var deviceName: String? = null,

        @SerializedName("distance")
        var distance: Float? = null

)