package com.social.app.api.services

import com.social.app.api.models.ProfileResponse
import com.social.app.api.models.alarms.NotificationRequest
import com.social.app.api.models.alarms.NotificationsModel
import com.social.app.api.models.alarms.PanicBody

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface RastreoApi {

    @GET("/device/{id}")
    fun getDevices(@Path("id") id: String): Observable<ProfileResponse>

    @GET("/api/getNotificationByGroup/{group}")
    fun getNotifications(@Path("group") group: Int): Single<List<NotificationsModel>>

    @POST("/api/createNotification")
    fun createNotification(@Body panicBody: PanicBody): Completable

    @POST("/api/sharePosition")
    fun sharePosition(@Body panicBody: PanicBody): Completable

    @POST("/api/deleteNotification")
    fun deleteNotification(@Body notificationsModel: NotificationsModel): Completable

}
