package com.social.app.api.models.alarms

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class NotificationsModel(

        @SerializedName("id_user")
        val idUser: String? = null,

        @SerializedName("names")
        val names: String? = null,

        @SerializedName("telephone")
        val telephone: String? = null,

        @SerializedName("latitude")
        val latitude: Float? = null,

        @SerializedName("longitude")
        val longitude: Float? = null,

        @SerializedName("id_notificacion")
        val idNotification: Int? = null,

        @SerializedName("status_notificacion")
        val statusNotification: Boolean = false,

        @SerializedName("created")
        val created: String? = null,

        @SerializedName("address")
        val address: String? = null,

        @SerializedName("comment")
        val comment: String? = null,

        @SerializedName("url_photo")
        val photo: String? = null,

        @SerializedName("id")
        val id: Int = 0,

        @SerializedName("visibility_system")
        val visibilitySystem: Boolean = true

) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Float::class.java.classLoader) as Float?,
            source.readValue(Float::class.java.classLoader) as Float?,
            source.readValue(Int::class.java.classLoader) as Int?,
            1 == source.readInt(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeValue(idUser)
        writeString(names)
        writeString(telephone)
        writeValue(latitude)
        writeValue(longitude)
        writeValue(idNotification)
        writeInt((if (statusNotification) 1 else 0))
        writeString(created)
        writeString(address)
        writeString(comment)
        writeString(photo)
        writeValue(id)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<NotificationsModel> = object : Parcelable.Creator<NotificationsModel> {
            override fun createFromParcel(source: Parcel): NotificationsModel = NotificationsModel(source)
            override fun newArray(size: Int): Array<NotificationsModel?> = arrayOfNulls(size)
        }
    }
}