package com.social.app.api.controllers

import com.social.base.preferences.PrefsManager
import com.social.app.api.models.ProfileResponse
import com.social.app.api.models.alarms.NotificationRequest
import com.social.app.api.models.alarms.NotificationsModel
import com.social.app.api.models.alarms.PanicBody
import com.social.app.api.services.RastreoApi
import com.social.app.managers.ProfileManager
import com.social.app.ui.GenericApplication
import com.social.app.utils.GeneralUtils
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RastreoApiController(private val rastreoApi: RastreoApi,
                           private val prefsManager: PrefsManager) {

    fun notifications(): Single<List<NotificationsModel>> =
            rastreoApi.getNotifications(ProfileManager.groupId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())


    fun isUserRegistered(telephone: String): Observable<ProfileResponse> {
        return rastreoApi.getDevices(telephone)
                .flatMap { response -> registerLoginSuccess(response, telephone) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError { this.loginError() }
    }

    private fun registerLoginSuccess(profileResponse: ProfileResponse, telephone: String): Observable<ProfileResponse> {
        profileResponse.phone = telephone
        ProfileManager.updateUserProfile(prefsManager, profileResponse)
        ProfileManager.setUserLogged()
        return Observable.just(profileResponse)
    }

    fun createNotification(notification: Int, address: String, latitude: Double, longitude: Double, visibilityString: Boolean? = true): Completable {

        val idUser = ProfileManager.userId
        val group = ProfileManager.groupId

        return rastreoApi.createNotification(PanicBody(idUser, latitude,
                longitude, notification, group, ProfileManager.userName, ProfileManager.telephone,
                GeneralUtils.getAndroidID(GenericApplication.get()), address, visibilitySystem = visibilityString))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun sharePosition(description: String, urlPhotoFirebase: String, address: String, notification: Int, latitude: Double, longitude: Double, visibilityString: Boolean? = true ): Completable {

        val idUser = ProfileManager.userId
        val group = ProfileManager.groupId

        return rastreoApi.sharePosition(PanicBody(idUser, latitude,
                longitude, notification, group, ProfileManager.userName, ProfileManager.telephone,
                GeneralUtils.getAndroidID(GenericApplication.get()), address, description, urlPhotoFirebase, visibilityString))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun deleteNotification(id: Int): Completable {
        return rastreoApi.deleteNotification(NotificationsModel(id = id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    private fun loginError() {

    }
}
