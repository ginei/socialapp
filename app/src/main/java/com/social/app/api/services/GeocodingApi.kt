package com.social.app.api.services

import com.social.app.api.models.geocoding.AddressModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface GeocodingApi {

    @GET("http://nominatim.openstreetmap.org/reverse")
    fun getAddress(@Query("format") format: String,
                   @Query("zoom") zoom: Int,
                   @Query("lat") lat: Double,
                   @Query("lon") lon: Double): Single<AddressModel>

    @GET("https://maps.googleapis.com/maps/api/geocode/json")
    fun getAddressGoogle(@Query("latlng") latlng: String,
                         @Query("key") key: String): Single<AddressModel>

}

