package com.social.app.api.services.platform

import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.PUT
import retrofit2.http.Url

interface LocationApi {

    @FormUrlEncoded
    @PUT
    fun setLocationToServer(@Url url: String,
                            @Field("id") id: String,
                            @Field("timestamp") timestamp: Long,
                            @Field("lat") lat: Double,
                            @Field("lon") lon: Double,
                            @Field("altitude") altitude: Double,
                            @Field("speed") speed: Double,
                            @Field("bearing") bearing: Double,
                            @Field("accuracy") accuracy: Double,
                            @Field("batt") batt: Double): Observable<Response<Void>>

}
