package com.social.app.utils

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.internal.Primitives
import java.lang.reflect.Type

fun <T> getObject(stringJson: String?, classObject: Class<T>): T {

    val gson: Gson?
    var `object`: Any? = null

    if (!stringJson.isNullOrEmpty()) {

        gson = GsonBuilder().serializeNulls().create()
        `object` = gson.fromJson<Any>(stringJson, classObject as Type)
    }

    return Primitives.wrap(classObject).cast(`object`)
}

fun objectToJson(obj: Any): String = Gson().toJson(obj)