package com.social.app.utils

import android.content.Intent
import android.net.Uri

import com.social.app.ui.GenericApplication

object AppUtils {

    fun callToNumber(number: String?) {
        if (number != null) {
            GenericApplication.get().startActivity(
                    Intent(Intent.ACTION_DIAL)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .setData(Uri.parse("tel:$number")))
        }
    }

}