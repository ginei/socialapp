package com.social.app.utils;

import android.location.Location;

public class DistanceUtils {

    public static Float getDistance(Location currentLocation, Location destinationLocation) {
        float distance;
        String distanceToShow = "0";
        if (destinationLocation != null && currentLocation != null) {

            distance = currentLocation.distanceTo(destinationLocation);

            distance = distance / 1000;

            distanceToShow = distance + "";
            if (distanceToShow.length() > 4) {
                distanceToShow = distanceToShow.substring(0, 4);
            }
            distanceToShow.replaceAll("\\.", ",");

        }

        return Float.valueOf(distanceToShow);
    }

    public static float distanceBetweenInKms(double startLatitude,
                                        double startLongitude,
                                        double endLatitude,
                                        double endLongitude) {

        float[] results = new float[1];

        Location.distanceBetween(
                startLatitude,
                startLongitude,
                endLatitude,
                endLongitude,
                results);
        return results[0]/1000;
    }


    public static int getDistanceInMeters(Location currentLocation, Location destinationLocation) {
        return (int) currentLocation.distanceTo(destinationLocation);
    }

    public static float getMetersToKilometers(float meters) {
        return round2(meters / 1000, 2);
    }

    private static float round2(float number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++)
            pow *= 10;
        float tmp = number * pow;
        return ((float) ((int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp))) / pow;
    }
}
