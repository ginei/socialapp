package com.social.app.utils

import android.content.Context
import com.social.app.R

const val PANIC = 1
const val COMPARTIR = 2
const val GPS_ON = 3
const val GPS_OFF = 4
const val VISITA = 5
const val CHECK_IN = 6
const val SERVICE_ON = 7
const val SERVICE_OFF = 8
const val START_VISIT = 9
const val END_VISIT = 10

fun Int.typeNotification(context: Context) : String{
    return when (this) {
        PANIC-> context.getString(R.string.copy_panic)
        COMPARTIR-> context.getString(R.string.copy_share)
        GPS_ON-> context.getString(R.string.copy_gps_on)
        GPS_OFF-> context.getString(R.string.copy_gps_off)
        VISITA-> context.getString(R.string.copy_visit)
        CHECK_IN-> context.getString(R.string.copy_check_in)
        SERVICE_ON-> context.getString(R.string.copy_service_on)
        SERVICE_OFF-> context.getString(R.string.copy_service_off)
        START_VISIT-> context.getString(R.string.copy_service_off)
        END_VISIT-> context.getString(R.string.copy_service_off)
        else -> ""
    }
}