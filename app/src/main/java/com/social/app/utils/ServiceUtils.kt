package com.social.app.utils

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import com.social.base.utils.AndroidVersionUtil

import com.social.app.ui.GenericApplication

object ServiceUtils {

    fun isServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = GenericApplication.get().applicationContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val nameServiceClass = serviceClass.name
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (nameServiceClass == service.service.className) {
                return true
            }
        }
        return false
    }

    fun stopService(serviceClass: Class<*>) {
        val context = GenericApplication.get().applicationContext
        if (isServiceRunning(serviceClass)) {
            context.stopService(Intent(context, serviceClass))
        }
    }

    fun startService(serviceClass: Class<*>) {
        if (!isServiceRunning(serviceClass)) {
            GenericApplication.get().applicationContext.startService(Intent(GenericApplication.get().applicationContext, serviceClass))
        }
    }

    fun restartService(serviceClass: Class<*>) {
        val context = GenericApplication.get().applicationContext
        stopService(serviceClass)
        if (AndroidVersionUtil.hasOreo())
            context.startForegroundService(Intent(context, serviceClass))
        else context.startService(Intent(context, serviceClass))
    }

}
