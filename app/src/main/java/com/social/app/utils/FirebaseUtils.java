package com.social.app.utils;

import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.social.base.preferences.PrefsManager;
import com.social.base.utils.LogUtil;
import com.social.app.managers.preferences.ConstantPreferences;
import com.social.app.ui.GenericApplication;

import java.util.HashSet;
import java.util.Set;

public class FirebaseUtils {

    private final static Set<String> subscribedChannels = new HashSet<>();
    private final static String BROADCAST_CHANNEL = "rastreo";
    private final static String CLIENT_CHANNEL = "rastreo_user_";
    private static FirebaseUtils instance;
    private String channel;

    private FirebaseUtils() {
        FirebaseApp.initializeApp(GenericApplication.get());
    }

    public static FirebaseUtils getInstance() {
        if (instance == null) {
            instance = new FirebaseUtils();
        }
        return instance;
    }

    private void registerBaseChannel() {
        subscribeTopic(BROADCAST_CHANNEL);
    }

    public void registerClientChannel(String id) {
        if (id != null && !id.equals("0")) {
            channel = CLIENT_CHANNEL + id;
            subscribeTopic(channel);
        }
    }

    public void registerGroupChannel(String group) {
        if (group != null && !group.equals("0")) {
            channel = CLIENT_CHANNEL + group;
            subscribeTopic(channel);
        }
    }

    public void unregisterAllChannels() {
        try {
            for (String channel : subscribedChannels) {
                FirebaseMessaging.getInstance().unsubscribeFromTopic(channel);
            }
            subscribedChannels.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void unSubscribeTopic(String topicToUnsubscribe) {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(topicToUnsubscribe);
        subscribedChannels.remove(topicToUnsubscribe);
        LogUtil.e(FirebaseUtils.class.getName(), "Channel removed: " + topicToUnsubscribe);
    }

    private void subscribeTopic(String registerChannel) {
        registerChannel = getTopicWithCountry(registerChannel);
        FirebaseMessaging.getInstance().subscribeToTopic(registerChannel);
        subscribedChannels.add(registerChannel);
        LogUtil.e(FirebaseUtils.class.getName(), "Channel added: " + registerChannel);
    }

    private String getTopicWithCountry(String simpleTopic) {
        String registerTopicWithCountry = PrefsManager.getInstance().getString(ConstantPreferences.MY_COUNTRY_CODE);
        if (registerTopicWithCountry.length() > 0) {
            registerTopicWithCountry += "_" + simpleTopic;
        } else
            registerTopicWithCountry = simpleTopic;
        return registerTopicWithCountry;
    }

    public void initialize() {
        registerBaseChannel();
    }

    public void unsubscribeClientTopic(String userId) {
        String simpleTopic = CLIENT_CHANNEL + userId;
        String topicWithCountry = getTopicWithCountry(simpleTopic);
        unSubscribeTopic(topicWithCountry);
    }

    public void unsubscribeFromAllChannels() {
        for (String channel : subscribedChannels)
            FirebaseMessaging.getInstance().unsubscribeFromTopic(channel);
    }

}
