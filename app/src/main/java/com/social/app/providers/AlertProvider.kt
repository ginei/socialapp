package com.social.app.providers

import com.social.app.api.models.alarms.NotificationsModel
import com.social.app.managers.ProfileManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject

class AlertProvider {

    private val alertList: BehaviorSubject<NotificationProvider<ArrayList<NotificationsModel>>> =
            BehaviorSubject.createDefault(NotificationProvider(ArrayList()))

    fun updateAlertList(registerModel: List<NotificationsModel>) {
        val notificationProvider = alertList.value
        notificationProvider?.let {
            it.setData(registerModel)
            alertList.onNext(it)
        }
    }

    fun getAlertList(): ArrayList<NotificationsModel>? =
            alertList.value?.`object`

    fun getGroupAlertList(): List<NotificationsModel> {
        val alerts = alertList.value?.`object`
        return alerts?.filter { it.idUser != ProfileManager.userId }
                ?: arrayListOf(NotificationsModel())
    }

    fun getMyAlertList(): List<NotificationsModel> {
        val alerts = alertList.value?.`object`
        return alerts?.filter { it.idUser == ProfileManager.userId }
                ?: arrayListOf(NotificationsModel())
    }

    fun onAlertListChange(): Observable<NotificationProvider<ArrayList<NotificationsModel>>> =
            alertList.observeOn(AndroidSchedulers.mainThread())
}