package com.social.app.providers;

import android.content.Context;
import androidx.annotation.StringRes;
import android.widget.Toast;

public class ToastManager {

    private Context context;

    public ToastManager(Context context) {
        this.context = context;
    }

    public void show(@StringRes int stringId) {
        Toast.makeText(context, stringId, Toast.LENGTH_SHORT).show();
    }

    public void show(@StringRes int stringId, Object...args) {
        Toast.makeText(context, context.getString(stringId, args), Toast.LENGTH_SHORT).show();
    }
}
