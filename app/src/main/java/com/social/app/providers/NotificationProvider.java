package com.social.app.providers;

import androidx.annotation.NonNull;

public class NotificationProvider<T> {

    private T objectNotify;

    private Throwable error;

    public NotificationProvider(T objectNotify) {
        this.objectNotify = objectNotify;
    }

    public NotificationProvider(Throwable error) {
        this.error = error;
    }

    public boolean hasError() {
        return error != null;
    }

    public void setError(Throwable t) {
        error = t;
    }

    public void setInformation(T newInformation) {
        objectNotify = newInformation;
        clearError();
    }

    public void clearError() {
        error = null;
    }

    public void setData(@NonNull Object object) {
        if (object instanceof Throwable) {
            setError((Throwable) object);
        } else {
            setInformation((T) object);
        }
    }

    public T getObject() {
        return objectNotify;
    }

    public Throwable getError() {
        return error;
    }
}

